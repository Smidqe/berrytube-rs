use tui::{
	Terminal,
	backend::{CrosstermBackend},
	layout::{Layout, Constraint, Direction},
	widgets::Widget,
};

use crate::berrytube::session::Session;

use std::sync::Arc;
use std::io::{self, Write, stdout};

use parking_lot::Mutex;

use crate::{
	Shared,
	error::{Error, Errors},
};

use crate::elements::{
	ChatElement,
	PlaylistElement,
	PollsElement,
	InputElement,
	UserlistElement,
	StatusbarElement
};


use crossterm::{
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};


#[derive(PartialEq, Eq, Hash, Clone, Copy)]
pub enum Block {
	None,
	Chat,
	Polls,
	Playlist,
	Userlist,
}

impl From<&str> for Block {
	fn from(s: &str) -> Block {
		match s {
			"chat" => Block::Chat,
			"polls" => Block::Polls,
			"playlist" => Block::Playlist,
			"users" => Block::Userlist,

			_ => Block::None
		}
	}
}

impl From<&&str> for Block {
	fn from(s: &&str) -> Block {
		Block::from(*s)
	}
}


pub(crate) type Term = Terminal<CrosstermBackend<io::Stdout>>;

pub struct UI {
	pub terminal: Term,
	pub blocks: (Block, Block),

	//elements
	pub chat: Shared<ChatElement<'static>>,
	pub input: Shared<InputElement<'static>>,
	pub playlist: Shared<PlaylistElement>,
	pub polls: Shared<PollsElement<'static>>,
	pub userlist: Shared<UserlistElement<'static>>,
	pub statusbar: Shared<StatusbarElement>,
	/*
	pub elements: HashMap<Block, Shared<dyn Widget + Controls>>
	pub io: Sender<Action>
	*/
}

impl UI {
	pub fn new() -> Result<UI, Error> {
		enable_raw_mode().map_err(|_| Error::new(Errors::Custom, "Failed to enable raw mode"))?;

		let mut stdout = io::stdout();

		match execute!(stdout, EnterAlternateScreen) {
			Ok(_) => (),
			Err(e) => warn!("Error: {}", e)
		};

		let backend = CrosstermBackend::new(stdout);
		let mut terminal = Terminal::new(backend)?;

		terminal.clear()?;

		let input = Arc::new(Mutex::new(
			InputElement::default()
		));

		let playlist = Arc::new(Mutex::new(
			PlaylistElement::new()
		));

		let polls = Arc::new(Mutex::new(
			PollsElement::new()
		));

		let users = Arc::new(Mutex::new(
			UserlistElement::new()
		));


		let chat = Arc::new(Mutex::new(
			ChatElement::new(input.clone())
		));

		let statusbar = chat.lock().status.clone();

		Ok(UI {
			terminal,
			chat,
			playlist,
			polls,
			blocks: (Block::Chat, Block::None),
			input,
			userlist: users,
			statusbar
		})
	}

	pub fn set_cursor(&mut self, cursor: (u16, u16)) -> Result<(), Error> {
		self.terminal.set_cursor(cursor.0, cursor.1).map_err(|_| Error::new(Errors::Io, "Failed to set the cursor"))
	}

	pub fn stop(&mut self) -> Result<(), Error> {
		disable_raw_mode().map_err(|_| Error::new(Errors::Io, "Failed to disable raw mode"))?;

		execute!(
			stdout(),
			LeaveAlternateScreen
		).map_err(|_| Error::new(Errors::Custom, "Failed to exit alternate screen"))
	}

	pub fn render(&mut self) -> Result<(), Error> {
		let size = self.terminal.size()?;
		let constraints = if self.blocks.1 == Block::None {
			vec![Constraint::Percentage(100)]
		} else {
			vec![
				Constraint::Percentage(75),
				Constraint::Percentage(25)
			]
		};

		let areas = Layout::default()
			.constraints(constraints.as_ref())
			.direction(Direction::Horizontal)
			.split(size);

		//clone all arc's due to them requiring unique access inside a closure
		let blocks = self.blocks;
		let chat = self.chat.clone();
		let userlist = self.userlist.clone();
		let polls = self.polls.clone();
		let playlist = self.playlist.clone();

		self.terminal.draw(|mut f| {
			match blocks.0 {
				_ => chat.lock().render(&mut f, areas[0]),
			}

			match blocks.1 {
				Block::Userlist => userlist.lock().render(&mut f, areas[1]),
				Block::Polls => polls.lock().render(&mut f, areas[1]),
				Block::Playlist => playlist.lock().render(&mut f, areas[1]),
				_ => ()
			}

		}).map_err(|_| Error::new(Errors::Custom, "Failed to render"))?;


		Ok(())
	}

	pub fn handle_command(&mut self, _: &mut Session, args: Vec<String>) -> Result<(), Error> {
		let args: Vec<&str> = args.iter().map(|p| p.as_ref()).collect();

		match args.as_slice() {
			[_, "s"] => self.blocks.1 = Block::None,
			[_, "s", side] => {
				self.blocks.1 = Block::from(side);
				//send a mesasge
			},

			//main (chat area) area patterns
			[_] => self.blocks.0 = Block::None,
			[_, "m", main] => self.blocks.0 = Block::from(main),

			_ => ()
		};

		Ok(())
	}
}