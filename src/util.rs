pub fn time_str(ms: u128, sep: char) -> String {
	let len = ms as f64;
	let parts = vec![
		(len / 3600.0).floor() as usize,
		(len / 60.0).floor() as usize,
		(len % 60.0).floor() as usize
	];

	parts.iter().enumerate().map(|(index, part)| {
		if index == 0 && *part == 0 {
			return String::default();
		}

		let mut number = part.to_string();

		if (0..=9).contains(part) {
			number.insert(0, '0');
		}

		number
	}).filter(|p| !p.is_empty()).collect::<Vec<String>>().join(&sep.to_string())
}