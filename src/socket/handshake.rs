
use crate::error::{Error, Errors};
use std::net::SocketAddr;

pub struct Handshake {
	pub addr: SocketAddr,
	pub sid: String,
	pub origin: String,
	pub namespace: String,
	pub heartbeat: u64
}

impl Handshake {
	pub fn perform(origin: &str, namespace: &str) -> Result<Handshake, Error> {
		let response = reqwest::blocking::get(format!("https://{}/{}/1/", origin, namespace).as_str())?;

		let addr = match response.remote_addr() {
			Some(addr) => addr,
			None => return Err(Error::new(Errors::Io, "Remote address doesn't exist"))
		};

		match response.text()?.split(':').collect::<Vec<&str>>().as_slice() {
			[sid, heartbeat, _, transports] => {
				if !transports.contains("websocket") {
					return Err(Error::new(Errors::Io, "Websocket is not supported"))
				}

				Ok(Handshake {
					addr,
					origin: origin.to_string(),
					namespace: namespace.to_string(),
					sid: (*sid).to_string(),
					heartbeat: heartbeat.parse()?
				})
			},
			_ => Err(Error::new(Errors::Io, "Invalid response structure"))
		}
	}
}