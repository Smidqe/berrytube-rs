use std::time::{Instant, Duration};
use std::thread;
use std::convert::TryFrom;

use crossbeam_channel::Sender;

use super::websocket::Websocket;
use super::packet::Packet;
use super::Packets;
use super::handshake::Handshake;

use crate::error::{Error, Errors};
use std::thread::JoinHandle;

use tungstenite::{
	protocol::{
		frame::coding::CloseCode,
		CloseFrame
	}
};

use crate::Action;

use std::sync::Arc;
use parking_lot::Mutex;

/*
This file houses the necessary adapter between Berrytube and Rust

TODO:
	- Allow manual connecting to improve start times and handling of reconnects

*/
pub struct SocketIO {
	socket: Arc<Mutex<Websocket>>,

	//threads
	threads: Option<(JoinHandle<()>, JoinHandle<()>)>
	//handshake: Handshake
}

impl SocketIO {
	/*
	pub fn new()
	pub fn handshake
	pub fn connect(&mut self, origin: &str, namespace: &str, tx: Sender<Action>) -> Result<(), Error> {
		self.handshake = Handshake::perform(origin, namespace)?;

		let socket_url = format!("wss://{}/{}/1/websocket/{}", origin, namespace, handshake.sid);
		let ws = Websocket::new(&socket_url, &handshake.addr)?;
		let tcp_stream = ws.tcp();
		let duration = Duration::from_millis(50);

		tcp.set_write_timeout(Some(dur))?;
		tcp.set_read_timeout(Some(dur))?;

		self.socket = Arc::new(Mutex::new(ws));
		self.thread = thread::spawn(move || {
			loop {
				let now = Instant::now();

				match socket.lock().socket_mut().read_message() {
					Ok(msg) => {
						if let Ok(packet) = Packet::try_from(msg.into_data().as_slice()) {
							tx.send(Action::Event(packet));
						}
					},
					Err(tungstenite::Error::AlreadyClosed) |
					Err(tungstenite::Error::ConnectionClosed) => {
						break;
					},
					Err(tungstenite::Error::Io(err)) => {
						match err.kind() {
							std::io::ErrorKind::WouldBlock => (),
							_ => warn!("SocketIO: IO Error: {:?}", err.kind())
						}
					}
					Err(_) => ()
				}

				if now.duration_since(ping.1).as_millis() > ping.0 {
					let packet = tungstenite::Message::from(Packet::new(Packets::Heartbeat, &[]).to_string());

					match socket.lock().socket_mut().write_message(packet) {
						Ok(_) => (),

						Err(tungstenite::Error::AlreadyClosed) |
						Err(tungstenite::Error::ConnectionClosed) |
						Err(_) => break
					};

					ping.1 = now;
				}
			}
		})

		Ok(())
	}
	*/

	pub fn new(origin: &str, namespace: &str, tx: Sender<Action>) -> Result<SocketIO, Error> {
		//this could be
		let handshake = Handshake::perform(origin, namespace)?;

		let socket_url = format!("wss://{}/{}/1/websocket/{}", origin, namespace, handshake.sid);
		let ws = Websocket::new(socket_url.as_str(), &handshake.addr)?;

		let tcp = ws.tcp();
		let dur = Duration::from_millis(100);

		tcp.set_write_timeout(Some(dur))?;
		tcp.set_read_timeout(Some(dur))?;

		let socket = Arc::new(Mutex::new(ws));

		//copy necessary values
		let thread_socket = socket.clone();

		let heartbeat = thread::Builder::new().name(String::from("socket heartbeat")).spawn(move || {
			let mut ping = (handshake.heartbeat / 2, Instant::now());
			let socket = thread_socket;

			loop {
				let now = Instant::now();

				if now.duration_since(ping.1).as_millis() > u128::from(ping.0) {
					//acquire lock for websocket
					let packet = Packet::new(Packets::Heartbeat, &[]).to_string();

					match socket.lock().socket_mut().write_message(tungstenite::Message::from(packet)) {
						Ok(_) => (),
						Err(tungstenite::Error::AlreadyClosed) |
						Err(tungstenite::Error::ConnectionClosed) => break,
						Err(_) => {
							break
						},
					};

					ping.1 = now;
				}

				thread::sleep(Duration::from_millis(500))
			}
		})?;

		let thread_socket = socket.clone();
		let event_loop = thread::spawn(move || {
			loop {
				match thread_socket.lock().socket_mut().read_message() {
					Ok(message) => {
						warn!("Got a message from Socket.IO: {}", message);
						let _sent = match Packet::try_from(message.into_data().as_slice()) {
							Ok(packet) => tx.send(Action::Event(packet)),
							_ => Ok(())
						};
					},
					Err(tungstenite::Error::AlreadyClosed) |
					Err(tungstenite::Error::ConnectionClosed) => {
						break;
					},
					Err(tungstenite::Error::Io(err)) => {
						if err.kind() != std::io::ErrorKind::WouldBlock {
							warn!("Got an IO error: {}\r", err);
						}
					}
					Err(_) => ()
				}
			}
		});

		Ok(SocketIO {
			socket,
			threads: Some((heartbeat, event_loop))
		})
	}

	pub fn stop(&mut self) -> Result<(), Error> {
		self.socket.lock().socket_mut().close(Some(CloseFrame {
			code: CloseCode::Normal,
			reason: "Graceful shutdown".into()
		})).map_err(|_| Error::new(Errors::Shutdown, "Failed to close socket"))?;

		if let Some(threads) = self.threads.take() {
			threads.0.join().map_err(|_| Error::new(Errors::Shutdown, "Failed to close heartbeat thread"))?;
			threads.1.join().map_err(|_| Error::new(Errors::Shutdown, "Failed to close events thread"))?;
		}

		Ok(())
	}

	pub fn send(&mut self, packet: Packet) -> Result<(), Error> {
		self.socket
			.lock()
			.socket_mut()
			.write_message(packet.into())
			.map_err(|_| Error::new(Errors::Io, "Failed to send message to websocket"))
	}
}
