use url::Url;

use std::net::{TcpStream, SocketAddr};
use openssl::ssl::{SslStream, SslConnector, SslMethod, SslVerifyMode};

use crate::error::{Error, Errors};


pub type Stream = SslStream<TcpStream>;

pub struct Websocket {
	socket: tungstenite::WebSocket<Stream>,
	tcp: TcpStream
}

impl Websocket {
	pub fn new(path: &str, port: &SocketAddr) -> Result<Websocket, Error> {
		//create connector
		let mut builder = SslConnector::builder(SslMethod::tls())?;

		//don't verify the SSL certificate
		//as otherwise we get 'host mismatch'
		builder.set_verify(SslVerifyMode::NONE);

		let connector = builder.build();

		//create tcpstream and connect to it
		let tcp = TcpStream::connect(port)?;
		let copy_tcp = tcp.try_clone()?;

		let stream = connector.connect(path, tcp)?;
		let (client, response) = tungstenite::client(Url::parse(path)?, stream)?;

		if response.status().as_u16() != 101 {
			return Err(Error::new(Errors::Stream, format!("Invalid response: 101 != {}", response.status().as_u16()).as_str()));
		}

		Ok(Websocket {
			socket: client,
			tcp: copy_tcp
		})
	}

	pub fn socket(&self) -> &tungstenite::WebSocket<Stream> {
		&self.socket
	}

	pub fn socket_mut(&mut self) -> &mut tungstenite::WebSocket<Stream> {
		&mut self.socket
	}

	pub fn tcp(&self) -> &TcpStream {
		&self.tcp
	}

	pub fn tcp_mut(&mut self) -> &mut TcpStream {
		&mut self.tcp
	}
}