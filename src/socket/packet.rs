use std::fmt::Display;
use std::convert::TryFrom;

use crate::error::{Errors, Error};
use super::Packets;

#[derive(Debug, Clone)]
pub struct Packet {
	pub id: Packets,
	pub data: Vec<u8>
}

impl Packet {
	pub fn new(id: Packets, data: &[u8]) -> Packet {
		Packet {
			id,
			data: data.to_vec()
		}
	}
}

impl Display for Packet {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		write!(fmt, "{}::", self.id.as_u8())?;

		if !self.data.is_empty() {
			write!(fmt, ":{}", String::from_utf8(self.data.clone()).unwrap_or_else(|_| String::from("")))?;
		}

		Ok(())
	}
}

impl TryFrom<&str> for Packet {
	type Error = Error;

	fn try_from(data: &str) -> Result<Packet, Error> {
		match data.to_owned().splitn(4, ':').collect::<Vec<&str>>().as_slice() {
			[id, _, _, data] => {
				Ok(Packet::new(Packets::try_from(*id)?, data.as_bytes()))
			},
			[id, _, _] => {
				Ok(Packet::new(Packets::try_from(*id)?, &[]))
			},
			_ => Err(Error::new(Errors::Custom, format!("Invalid packet format: {}, split: {:?}", data, data.to_owned().split(':').collect::<Vec<&str>>()).as_str()))
		}
	}
}

impl TryFrom<&[u8]> for Packet {
	type Error = Error;

	fn try_from(data: &[u8]) -> Result<Packet, Error> {
		Packet::try_from(String::from_utf8(data.to_vec())?.as_str())
	}
}

impl Into<tungstenite::Message> for Packet {
	fn into(self) -> tungstenite::Message {
		tungstenite::Message::from(self.to_string())
	}
}