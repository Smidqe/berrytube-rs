pub mod adapter;
pub mod packet;
pub mod websocket;
pub mod handshake;

use std::convert::TryFrom;
use crate::error::{Error, Errors};

#[derive(Debug, Clone, PartialEq)]
pub enum Packets {
	Disconnect,
	Connect,
	Heartbeat,
	Message,
	Json,
	Event,
	Ack,
	Error,
	Noop
}

impl Packets {
	fn as_u8(&self) -> u8 {
		match *self {
			Packets::Disconnect => 0,
			Packets::Connect => 1,
			Packets::Heartbeat => 2,
			Packets::Message => 3,
			Packets::Json => 4,
			Packets::Event => 5,
			Packets::Ack => 6,
			Packets::Error => 7,
			Packets::Noop => 8
		}
	}
}

impl TryFrom<u8> for Packets {
	type Error = Error;

	fn try_from(n: u8) -> Result<Packets, Error> {
		match n {
			0 => Ok(Packets::Disconnect),
			1 => Ok(Packets::Connect),
			2 => Ok(Packets::Heartbeat),
			3 => Ok(Packets::Message),
			4 => Ok(Packets::Json),
			5 => Ok(Packets::Event),
			6 => Ok(Packets::Ack),
			7 => Ok(Packets::Error),
			8 => Ok(Packets::Noop),

			_ => Err(Error::new(Errors::InvalidPacket, "Invalid packet ID from u8"))
		}
	}
}

impl TryFrom<&str> for Packets {
	type Error = Error;

	fn try_from(txt: &str) -> Result<Packets, Error> {
		match txt {
			"0" => Ok(Packets::Disconnect),
			"1" => Ok(Packets::Connect),
			"2" => Ok(Packets::Heartbeat),
			"3" => Ok(Packets::Message),
			"4" => Ok(Packets::Json),
			"5" => Ok(Packets::Event),
			"6" => Ok(Packets::Ack),
			"7" => Ok(Packets::Error),
			"8" => Ok(Packets::Noop),

			_ => Err(Error::new(Errors::InvalidPacket, "Invalid packet ID from &str"))
		}
	}
}
