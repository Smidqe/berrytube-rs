use crate::socket::packet::Packet;
use crossterm::event::KeyEvent;
use tui::layout::Rect;
use crate::parser::Event;
use crate::error::Error;
use crate::berrytube::session::Session;

#[derive(Debug, Clone)]
pub enum Action {
	Render,
	Key(KeyEvent),
	Message(String),
	Event(Packet),
	Mode(Mode),
	Cursor((u16, u16)),
	Shutdown,
	InputBuffer(String),
	DPM(f64),
	Clear
}

#[derive(PartialEq, Debug, Clone)]
pub enum Mode {
	Normal,
	Chat,
	Command,
}

impl Default for Mode {
	fn default() -> Mode {
		Mode::Normal
	}
}

impl ToString for Mode {
	fn to_string(&self) -> String {
		let name = match *self {
			Mode::Normal => "Normal",
			Mode::Command => "Command",
			Mode::Chat => "Chat",
		};

		name.to_string()
	}
}

pub trait Dimensions {
	fn resize(&mut self, area: Rect);
	fn width(&self) -> usize;
	fn height(&self) -> usize;
}

pub trait Handler {
	fn handle_event(&mut self, session: &mut Session, event: Event) -> Result<(), Error>;
	fn handle_command(&mut self, session: &mut Session, args: &[String]) -> Result<(), Error>;
}

pub trait Controllable {
	fn scroll(&mut self, direction: usize, amount: usize);
	fn focus(&mut self);
	fn unfocus(&mut self);
}