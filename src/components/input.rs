use std::{
	thread::{self, JoinHandle},
};

use crate::components::viewbuffer::ViewBuffer;

use crossterm::event::{self, KeyCode, Event, KeyModifiers};
use crossbeam_channel::Sender;
use crate::{
	Action,
	globals::{Mode, Dimensions}
};

use std::time::Duration;
use crate::{
	Shared,
	elements::InputElement
};

pub struct Input {
	pub listener: Option<JoinHandle<()>>,
	pub element: Shared<InputElement<'static>>,
	pub sender: Sender<Action>,
}

impl Input {
	pub fn new(io: Sender<Action>, element: Shared<InputElement<'static>>) -> Input {
		Input {
			listener: None,
			sender: io,
			element
		}
	}

	pub fn set_visible_str(&mut self, text: &str) {
		self.element.lock().set_raw_text(text.to_string())
	}

	pub fn element(&self) -> Shared<InputElement<'static>> {
		self.element.clone()
	}

	pub fn run(&mut self) {
		let io = self.sender.clone();
		let element = self.element.clone();

		let handle = thread::spawn(move || {
			let mut events = Vec::new();
			let mut shutdown = false;

			let mut buffers = (ViewBuffer::default(), ViewBuffer::default());
			let mut mode = Mode::Normal;

			loop {
				let buffer = match mode {
					Mode::Command | Mode::Normal => &mut buffers.0,
					Mode::Chat => &mut buffers.1,
					//Mode::Log => &mut buffers.2
				};

				if let Ok(false) = event::poll(Duration::from_millis(50)) {
					thread::yield_now();
					continue;
				}

				if let Ok(event) = event::read() { //&& !disabled
					let width = element.lock().width();

					if width != buffer.get_max_width() {
						buffer.set_max_width(width);
					}

					if let Event::Key(ev) = event {
						let key = ev.code;
						let modifiers = ev.modifiers;

						//send the key action always
						events.push(Action::Key(ev));

						match key {
							KeyCode::Esc => {
								if mode == Mode::Chat {
									events.push(Action::Mode(Mode::Normal));
									mode = Mode::Normal;
								}
							},
							KeyCode::Enter => {
								shutdown = buffer.buffer() == ":q" && mode == Mode::Command;

								if shutdown {
									events.push(Action::Shutdown);
								} else {
									events.push(Action::Message(buffer.buffer().to_string()));
								}

								if mode == Mode::Command {
									events.push(Action::Mode(Mode::Normal));
									mode = Mode::Normal;
								}

								buffer.reset();
							},
							KeyCode::Char(c) => {
								let (change, next_mode) = match c {
									':' => {
										(buffer.buffer.is_empty() && mode == Mode::Normal, Mode::Command)
									},
									'i' | 'w' => {
										(buffer.buffer.is_empty() && mode != Mode::Chat, Mode::Chat)
									}
									_ => (false, mode.clone())
								};

								let modify = !change || next_mode == Mode::Command;

								if change {
									events.push(Action::Mode(next_mode.clone()));
									mode = next_mode;
								}

								if modify {
									if c == 'h' && modifiers == KeyModifiers::CONTROL {
										buffer.remove();

										if buffer.is_empty() && mode == Mode::Command {
											events.push(Action::Mode(Mode::Normal));
											mode = Mode::Normal;
										}
									} else {
										buffer.push(c);
									}
								}
							},
							KeyCode::Backspace => {
								buffer.remove();

								if buffer.is_empty() && mode != Mode::Normal {
									events.push(Action::Mode(Mode::Normal));
									mode = Mode::Normal;
								}
							},
							KeyCode::Left | KeyCode::Right => {
								match key {
									KeyCode::Right => buffer.move_cursor_forward(1),
									KeyCode::Left => buffer.move_cursor_back(1),
									_ => ()
								}
							},
							_ => ()
						}
					}

					events.extend_from_slice(&[
						Action::InputBuffer(buffer.get_visible_str().to_string()),
						Action::Render,
						Action::Cursor((
							buffer.cursor().1 as u16 + 1, element.lock().area.top()
						))
					]);
				}

				events.drain(..).for_each(|event| {
					match io.send(event) {
						Ok(_) => (),
						Err(e) => warn!("IO: Send: {}", e)
					};
				});

				if shutdown {
					break;
				}
			}
		});

		self.listener = Some(handle);
	}
}
