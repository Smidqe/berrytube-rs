use tui::{
	widgets::Widget,
	layout::{Rect, Direction},
	buffer::Buffer,

};

/*

TODO:
	Depending on the orientation of the component (Horizontal / Vertical)
	We either utilise │ or ─


*/


pub struct Splitter {
	direction: Direction
}

impl Splitter {
	pub fn new() -> Splitter {
		Splitter {
			direction: Direction::Horizontal
		}
	}
}

impl Widget for Splitter {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		if area.width == 0 || area.height == 0 {
			return;
		}

		let mut pos = (area.left(), area.top());

		match self.direction {
			Direction::Horizontal => pos.1 = area.height / 2,
			Direction::Vertical => pos.0 = area.width / 2
		};

		//draw the elements one by one
		let char = match self.direction {
			Direction::Horizontal => '\u{2501}',
			Direction::Vertical	=> '\u{2503}'
		};

		while pos.0 != area.width && pos.1 != area.height {
			buf.get_mut(pos.0, pos.1).set_char(char);

			match self.direction {
				Direction::Horizontal => pos.0 += 1,
				Direction::Vertical => pos.1 += 1,
			}
		}
	}
}