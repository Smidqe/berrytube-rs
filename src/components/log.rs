use tui::widgets::Widget;
use tui::buffer::Buffer;
use tui::layout::Rect;
use tui::style::Style;
use tui::widgets::Text;

use crate::elements::{
	ScrollableList, Paragraph, Order
};

pub enum Level {
	Debug,
	Info,
	Error,
	Critical
}

pub struct Log<'b> {
	messages: ScrollableList<Paragraph<'b>>
}

impl <'b> Log<'b> {
	pub fn new() -> Log<'b> {
		Log {
			messages: ScrollableList::new(1000, Order::Normal)
		}
	}

	/*
	pub fn add(&mut self, message: &'b str, level: Level, file: bool) {



		if file {
			match level {
				Level::Debug => debug!()
				Level::Info => info!()
				Level::Warn => warn!()
				Level::Error => error!()
				Level::Critical => crit!(),
				Level::Trace => trace!(),
				Level::Warn => warn!()
			}
		}
	}
	*/

	pub fn add(&mut self, message: &'b str, level: Level) {
		let (flavor, style) = match level {
			Level::Debug => ("DEBUG", Style::default()),
			Level::Info => ("INFO", Style::default()),
			Level::Error => ("ERROR", Style::default()),
			Level::Critical => ("CRITICAL", Style::default())
		};

		let text = Paragraph::from_parts(&[
			Text::styled(flavor, style),
			Text::raw(":"),
			Text::raw(message)
		]);

		self.messages.add(text);
	}
}

impl <'b> Widget for Log<'b> {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		self.messages.draw(area, buf);
	}
}