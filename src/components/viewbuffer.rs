
#[derive(Default, Debug, Clone)]
pub struct ViewBuffer {
	//limit
	pub max_width: usize,
	//bounds
	pub bounds: (usize, usize),

	//normal, visual
	pub cursor: (usize, usize),
	pub buffer: String
}

impl ViewBuffer {
	pub fn push<T: ToString>(&mut self, string: T) {
		let string = string.to_string();

		if self.cursor.0 != self.buffer.len() {
			self.buffer.insert_str(self.cursor.0, string.as_str());
		} else {
			self.buffer.push_str(string.as_str());
		}

		self.cursor.0 += string.len();
		self.cursor.1 = std::cmp::min(self.cursor.1 + string.len(), self.max_width);

		//grow if necessary since we haven't hit the max width
		if self.width() < self.max_width {
			self.grow(string.len());
		}

		//shift if cursor is ahead of the boundaries, and we can't expand more
		if self.width() == self.max_width && self.cursor.0 > self.bounds.1 {
			self.shift(self.cursor.0 - self.bounds.1);
		}
	}

	pub fn len(&self) -> usize {
		self.buffer.len()
	}

	pub fn width(&self) -> usize {
		self.bounds.1 - self.bounds.0
	}

	pub fn remove(&mut self) {
		if self.buffer.is_empty() {
			return;
		}

		let is_shrinkable = self.buffer.len() < self.max_width;

		if self.cursor.0 == self.buffer.len() {
			self.buffer.pop();
			self.cursor.0 -= 1;
			self.unshift(1);

			if is_shrinkable {
				self.cursor.1 -= 1;
			}
		} else {
			self.buffer.remove(self.cursor.0);
		}

		if is_shrinkable {
			self.shrink(1);
		}
	}

	//shrink only from right
	pub fn shrink(&mut self, amount: usize) {
		if self.width() == 0 {
			return;
		}

		self.bounds.1 -= amount;
	}

	//grow, limit
	pub fn grow(&mut self, amount: usize) {
		let width = self.bounds.1 - self.bounds.0;

		if width == self.max_width {
			return;
		}

		if width + amount > self.max_width {
			self.bounds.1 += self.max_width - width;
		} else {
			self.bounds.1 += amount;
		}
	}

	pub fn shift(&mut self, amount: usize) {
		//calc max
		let max = self.buffer.len() - self.bounds.1;

		self.bounds.0 += std::cmp::min(amount, max);
		self.bounds.1 += std::cmp::min(amount, max);
	}


	pub fn unshift(&mut self, amount: usize) {
		if self.bounds.0 == 0 || amount > self.bounds.0 {
			return;
		}

		self.bounds.0 -= amount;
		self.bounds.1 -= amount;
	}

	pub fn bounds(&self) -> (usize, usize) {
		self.bounds
	}

	pub fn cursor(&self) -> (usize, usize) {
		self.cursor
	}

	pub fn set_max_width(&mut self, limit: usize) {
		self.max_width = limit;
	}

	pub fn get_max_width(&self) -> usize {
		self.max_width
	}

	pub fn buffer(&self) -> &str {
		self.buffer.as_str()
	}

	pub fn reset(&mut self) {
		self.bounds = (0, 0);
		self.cursor = (0, 0);
		self.buffer.clear();
	}

	pub fn move_cursor_forward(&mut self, amount: usize) {
		if self.cursor.0 + amount > self.buffer.len() || self.cursor.0 == self.buffer.len() {
			return;
		}

		self.cursor.0 += amount;
		self.cursor.1 += std::cmp::min(amount, self.max_width - self.cursor.1);

		if self.cursor.1 == self.max_width {
			self.shift(amount);
		}
	}

	pub fn move_cursor_back(&mut self, amount: usize) {
		if amount > self.cursor.0 || self.cursor.0 == 0 {
			return;
		}

		self.cursor.0 -= amount;

		if amount < self.cursor.1 {
			self.cursor.1 -= amount;
		} else {
			self.cursor.1 = 0;
		}

		if self.cursor.1 == 0 {
			self.unshift(amount);
		}
	}

	pub fn get_visible_str(&self) -> &str {
		&self.buffer[self.bounds.0..self.bounds.1]
	}

	pub fn is_empty(&self) -> bool {
		self.buffer.is_empty()
	}
}