use std::collections::HashMap;
use super::berrytube::session::Session;


/*
pub struct Command {
	name: String,
	args: Vec<(String, String)>
}

pub trait Command {
	fn run(&self, session: &Session) -> Result<()>;

}
*/

pub trait Command {
	fn run(&self, session: &Session, args: &[&str]);
}

pub enum Commands {
	Login,
	Logout,
	Search,
	Users,
	Sidebar
}

pub struct CommandPool {
	pub commands: HashMap<Commands, Box<dyn Command>>
}