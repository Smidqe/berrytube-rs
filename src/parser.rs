use crate::error::{Error, Errors};
use serde_json::{Value, json};
use serde::{Serialize};

/*
	TODO:
	- Finish the rest of events,
	-

*/

#[derive(Debug)]
pub enum Commands {
	Users,
	Playlist,
	Polls,
	Login,
	Help,
	Info,
	Video,
	Quit,
	UI,

	Unknown
}

#[derive(Debug, Clone)]
pub enum Events {
	Areas,
	Message,
	Drinks,
	Berry,
	Userlist,
	InitialisePlaylist,
	Join,
	Part,
	Type,
	Nick,
	Usercount,
	Toggleables,
	VideoAdd,
	VideoMove,
	VideoRemove,
	Volatility,
	VideoChange,
	PollCreate,
	PollUpdate,
	PollClose,

	AddPlaylist,	//Events::PlaylistAdd
	AddVideo,		//Events::VideoAdd
	ChatMsg, 		//Events::Message
	ClearPoll,		//Events::PollClear
	//CreatePlayer,
	Debug,
	DebugDump,
	DelVideo,
	DoorStuck,
	DrinkCount,
	DupeAdd,
	Error,
	FondleUser,
	//ForceRefresh,
	HBVideoDetail,
	Kicked,
	LeaderIs,
	LoginError,
	NewChatList,
	NewPoll,
	NumConnected,
	//OverrideCSS,
	Reconnect,
	Reconnecting,
	RecvBanList,
	RecvFilters,
	RecvNewPlaylist,
	RecvPlaylist,
	RecvPlugins,
	//RenewPos, ???
	SearchHistoryResults,
	ServerRestart, //need to handle this eventually
	//SetAreas,
	SetLeader,
	SetNick,
	//SetToggleable,
	//SetToggleables,
	SetType,
	//SetVidColorTag, //maybe later
	SetVidVolatile,
	Shadowban,
	//Shitpost,
	SortPlaylist,
	Unshadowban,
	UpdatePoll,
	UserJoin,
	UserPart,
	VideoRestriction,

	Unknown
}

impl From<&str> for Events {
	fn from(s: &str) -> Events {
		match s {
			"newChatList" => Events::Userlist,
			"numConnected" => Events::Usercount,
			"chatMsg" => Events::Message,
			"recvPlaylist" => Events::InitialisePlaylist,
			"userJoin" => Events::Join,
			"userPart" => Events::Part,
			"hbVideoDetail" => Events::HBVideoDetail,
			"setToggleables" => Events::Toggleables,
			"drinkCount" => Events::Drinks,
			"setAreas" => Events::Areas,
			"newPoll" => Events::PollCreate,
			"updatePoll" => Events::PollUpdate,
			"clearPoll" => Events::PollClose,
			"leaderIs" => Events::LeaderIs,
			"forceVideoChange" => Events::VideoChange,
			"sortPlaylist" => Events::VideoMove,
			"delVideo" => Events::VideoRemove,
			"addVideo" => Events::VideoAdd,
			_ => Events::Unknown
		}
	}
}

#[derive(Debug)]
pub struct Command {
	pub command: Commands,
	pub args: Vec<String>
}

#[derive(Debug)]
pub struct Event {
	pub id: Events,
	pub name: String,
	pub payload: Value
}

/*
	To distinguish from BT commands we will employ a vim like command format,
	just because I enjoy pain >:)

	Also these are not applicable if in insert/write mode
	just like in vim. So there'll be probably two modes, command and insert/write
	and i/w are the keys to enter that mode, esc to return to command mode

	:u [user | list | info] [berry | kick | ban | shadowban | search] -> user actions
	:h -> Help
	:i -> Info
	:l nick [pass] -> Login
	:p [vote [choice] [rank] | close | runoff | results | info | options] -> Poll actions
	:pl [active | info | next | prev | pos | search ] -> Playlist actions
	:v [title | length | info | source] ->
	:q -> Quit the program

	//UI based commands (gives a sidebar)
	:ui [s] [poll | playlist | users | none ] -> set sidebar, no args -> clear
	:ui [m] [chat | admin | log ] -> set main area element
	:ui [st] [set | clear] [drinks | time | video | source] -> set/clear statusbar elements (customisable)

	Of course normal BT commands that are started with /[..] are not included here
	as they are chat based commands and handled on the server

*/

pub struct Parser {}

impl Parser {
	pub fn parse_command(msg: &str) -> Result<Command, Error> {
		if msg.is_empty() {
			return Err(Error::new(Errors::Command, "Empty message"));
		}

		if msg.chars().nth(0).unwrap_or('\n') != ':' {
			return Err(Error::new(Errors::Command, "Not a command"))
		}

		let mut result = Command {
			command: Commands::Unknown,
			args: Vec::new()
		};

		result.args.extend(msg[1..].split(' ').map(|s| s.to_string()));
		result.command = match result.args[0].as_str() {
			"u" | "users" => Commands::Users,
			"h" | "help" => Commands::Help,
			"i" | "info" => Commands::Info,
			"l" | "login" => Commands::Login,
			"p" | "poll" => Commands::Polls,
			"pl" | "playlist" => Commands::Playlist,
			"v" | "video" => Commands::Video,

			//shortcut commands
			"vote" => Commands::Polls,

			//future commands ???
			/*
			"queue" | "q"
			""
			*/

			"ui" => Commands::UI,
			"q" => Commands::Quit,
			_ => Commands::Unknown
		};

		Ok(result)
	}

	pub fn decode_event(msg: &str) -> Result<Event, Error> {
		if msg.is_empty() {
			warn!("Event received was empty: {}", msg);
		}

		let json: Value = serde_json::from_str(msg)?;
		let event = json["name"].as_str().unwrap_or("");
		let id = Events::from(event);

		Ok(Event {
			id,
			name: event.to_owned(),
			payload: json["args"][0].to_owned()
		})
	}

	pub fn encode_event<T: Serialize>(event: &str, s: &T) -> Result<Value, Error> {
		let json = json!({
			"name": event,
			"args": [
				serde_json::to_value(s)?
			]
		});

		Ok(json)
	}
}