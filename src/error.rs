use std::error;
use std::fmt;
use std::io::{Read, Write};

use std::net::TcpStream;

use openssl::{
	error::ErrorStack,
	ssl
};

use tungstenite::{
	HandshakeError,
	ClientHandshake
};

#[derive(Debug)]
pub enum Errors {
	UrlParse,
	OpenSSL,
	OpenSSLHandshake,
	TungsteniteHandshake,
	Io,
	Reqwest,
	Custom,
	InvalidPacket,
	InvalidUtf8,
	Stream,
	JSON,
	Shutdown,
	Login,
	Command
}

/*
impl From<Errors>

*/

#[derive(Debug)]
pub struct Error {
	kind: Errors,
	msg: String
}

impl Error {
	pub fn new(id: Errors, reason: &str) -> Error {
		Error {
			kind: id,
			msg: String::from(reason)
		}
	}

	//pub fn set_message(&mut self, reason: &str) {}
}

impl fmt::Display for Error {
	fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
		write!(fmt, "Error: {}", self.msg)
	}
}

impl error::Error for Error {
	fn description(&self) -> &str {
		self.msg.as_str()
	}
}

impl From<url::ParseError> for Error {
	fn from(err: url::ParseError) -> Error {
		let msg = match err {
			_ => "error happened, messages not implemented yet"
		};

		Error {
			kind: Errors::UrlParse,
			msg: String::from(msg)
		}
	}
}

impl From<ErrorStack> for Error {
	fn from(err: ErrorStack) -> Error {
		let errors = err.errors().iter().map(|er| {
			er.reason().unwrap_or("[no reason]").to_string()
		}).collect::<Vec<String>>().join("|");

		Error {
			kind: Errors::OpenSSL,
			msg: errors
		}
	}
}

impl From<std::io::Error> for Error {
	fn from(err: std::io::Error) -> Error {
		Error {
			kind: Errors::Io,
			msg: format!("io: {:?}", err.kind())
		}
	}
}

impl From<ssl::HandshakeError<TcpStream>> for Error {
	fn from(err: ssl::HandshakeError<TcpStream>) -> Error {
		Error {
			kind: Errors::OpenSSLHandshake,
			msg: format!("{}", err),
		}
	}
}

impl <T> From<HandshakeError<ClientHandshake<T>>> for Error where T: Read + Write {
	fn from(_err: HandshakeError<ClientHandshake<T>>) -> Error {
		Error {
			kind: Errors::TungsteniteHandshake,
			msg: String::from("Tungstenite handshake failed")
		}
	}
}

impl From<reqwest::Error> for Error {
	fn from(err: reqwest::Error) -> Error {
		Error {
			kind: Errors::Reqwest,
			msg: format!("{}", err)
		}
	}
}

impl From<std::num::ParseIntError> for Error {
	fn from(_err: std::num::ParseIntError) -> Error {
		Error {
			kind: Errors::Custom,
			msg: String::from("Failed to convert string to number")
		}
	}
}

impl From<std::string::FromUtf8Error> for Error {
	fn from(_err: std::string::FromUtf8Error) -> Error {
		Error {
			kind: Errors::InvalidUtf8,
			msg: String::from("Invalid UTF-8 format")
		}
	}
}

impl From<serde_json::Error> for Error {
	fn from (err: serde_json::Error) -> Error {
		Error {
			kind: Errors::JSON,
			msg: format!(
				"Class: {:?}, line: {}, column: {}",
				err.classify(),
				err.line(),
				err.column()
			)
		}
	}
}