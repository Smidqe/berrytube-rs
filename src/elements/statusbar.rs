use percent_encoding::percent_decode_str;

use tui::style::Style;
use tui::layout::{Rect, Layout, Constraint, Direction};
use tui::buffer::Buffer;
use tui::widgets::Widget;

use crate::{
	Mode,
	util
};

/*
TODO:
	- Implement methods for controlling the shown statuses
	-

Structure
	- Calculate max values for these
	- Nick cannot be maxed so it's length + 2
	- same goes for drinks
	- Video title needs to be truncated if it's too long

State | Nick | drinks / dpm | elapsed / [Duration] - [video title]
*/

pub enum Statuses {
	Mode,
	Nick,
	Drinks,
	Elapsed,
	Video
}

pub struct StatusbarElement {
	pub users: usize,
	pub nick: String,
	pub video: (String, u128, u128),
	pub style: Style,
	pub drinks: (usize, f64),
	pub mode: Mode,
	pub dirty: bool,
	pub blocks: Vec<(usize, Rect)>
}

impl StatusbarElement {
	pub fn new() -> StatusbarElement {
		StatusbarElement {
			users: 0,
			nick: String::default(),
			video: (String::default(), 0, 0),
			style: Style::default(),
			drinks: (0, 0.0),
			mode: Mode::Normal,
			dirty: true,
			blocks: Vec::new(),
		}
	}

	pub fn set_users(&mut self, amount: usize) {
		self.users = amount;
		self.dirty = true;
	}

	pub fn set_mode(&mut self, mode: Mode) {
		self.mode = mode;
		self.dirty = true;
	}

	pub fn set_nick(&mut self, nick: &str) {
		self.nick = nick.to_string();
		self.dirty = true;
	}

	pub fn set_video_elapsed(&mut self, elapsed: u128) {
		self.video.2 = elapsed;
		self.dirty = true;
	}

	pub fn set_dpm(&mut self, dpm: f64) {
		self.drinks.1 = dpm;
		self.dirty = true;
	}

	pub fn update_video(&mut self, title: &str, length: u128, elapsed: u128) {
		self.video = (
			percent_decode_str(title).decode_utf8().unwrap().to_string(),
			length,
			elapsed
		);

		self.dirty = true;
	}

	pub fn update_drinks(&mut self, drinks: usize, dpm: f64) {
		self.drinks = (drinks, dpm);
		self.dirty = true;
	}

	pub fn rebuild(&mut self, area: Rect) {
		//don't rebuild if not needed
		if !self.dirty {
			return;
		}

		let constraints = vec![
			Constraint::Length(self.mode.to_string().len() as u16), //mode
			Constraint::Length(3),
			Constraint::Length(self.nick.len() as u16), //nick
			Constraint::Length(3),
			Constraint::Length(
				(util::time_str(self.video.1, ':').len() + util::time_str(self.video.2, ':').len() + 3) as u16 //time and elapsed of the video
			),
			Constraint::Length(3),
			Constraint::Length((self.drinks.0.to_string().len() + format!("{:.2}", self.drinks.1 as f64).len() + 3) as u16), //drinks and dpm
			Constraint::Length(3),
			Constraint::Max(area.width) //rest for the video length
		];

		self.blocks = Layout::default()
			.constraints(constraints)
			.direction(Direction::Horizontal)
			.horizontal_margin(1)
			.vertical_margin(0)
			.split(area)
			.iter()
			.enumerate()
			.map(|(index, area)| (index, *area))
			.collect();
	}
}

impl Widget for StatusbarElement {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		self.rebuild(area);

		let mut blocks = self.blocks.clone();

		blocks.iter_mut().for_each(|(index, area)| {
			//utilise get symbol between each string
			let string = match index {
				0 => self.mode.to_string(),
				2 => self.nick.clone(),
				4 => {
					vec![
						util::time_str(self.video.2 as u128, ':'),
						util::time_str(self.video.1 as u128, ':')
					].join(" - ")
				},
				6 => vec![
					self.drinks.0.to_string(),
					format!("{:.2}", self.drinks.1 as f64),
				].join(" / "),
				8 => self.video.0.clone(),
				_ => String::from(" | ")
			};

			if *index != 4 {
				buf.set_string(area.x, area.y, string, Style::default())
			} else {
				buf.set_stringn(area.x, area.y, string, area.width as usize, Style::default());
			}
		})
	}
}