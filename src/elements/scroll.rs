
use tui::{
	widgets::Widget,
	layout::Rect,
	buffer::Buffer,
};

pub enum Scroll {
	Up,
	Down,
}

#[derive(Clone)]
pub struct Scrollbar {
	pub pages: usize,
	pub page: usize,
	pub symbols: Vec<char>,
	pub area: Rect
}

impl Scrollbar {
	pub fn new() -> Scrollbar {
		Scrollbar {
			pages: 1,
			page: 0,
			symbols: vec![
				'\u{2588}', // full
				'\u{2587}', // 7/8
				'\u{2586}', // 6/8
				'\u{2585}', // 5/8
				'\u{2584}', // half
				'\u{2583}', // 3/8
				'\u{2582}', // 2/8
				'\u{2581}',  // 1/8
				'\u{2502}'	// stick
			],
			area: Rect::default()
		}
	}

	pub fn set_pages(&mut self, pages: usize) {
		self.pages = pages;
	}

	pub fn set_area(&mut self, area: Rect) {
		self.area = area;
	}

	pub fn calculate(&self) -> Vec<char> {
		let height = self.area.height as usize;
		let mut full = if height > self.pages {
			height - self.pages
		} else {
			0
		};

		//also take into account our current page
		let mut result = Vec::new();

		//from bottom to top, utilise full blocks first
		for _ in (self.area.y as usize)..=height {
			if full > 0 {
				result.insert(0, self.symbols[0]);
				full -= 1;
			} else {
				result.insert(0, self.symbols[8]);
			}
		};

		result
	}
}

impl Widget for Scrollbar {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		if area.width == 0 {
			return;
		}

		self.calculate().iter().enumerate().for_each(|(index, symbol)| {
			buf.get_mut(area.left(), area.top() + index as u16).set_char(*symbol);
		})
	}
}

