use tui::{
	widgets::{Widget, Block, Text, Borders},
	style::Style,
	layout::{Rect, Constraint},
	buffer::Buffer
};

use crate::globals::Dimensions;
use super::{
	ScrollableList, Paragraph, Order
};
use std::borrow::Cow;

#[derive(Clone)]
pub struct UserElement<'b> {
	pub style: Style,
	pub name: Paragraph<'b>,
	pub group: i8,
}

impl <'b> UserElement<'b> {
	pub fn new(name: &str, group: i8) -> UserElement<'b> {
		UserElement {
			name: Paragraph::from(name.to_owned()),
			style: Style::default(),
			group
		}
	}

	pub fn set_style(&mut self, style: Style) {
		if let Some(Text::Styled(_, s)) = self.name.part(0) {
			s.fg(style.fg);
			s.bg(style.bg);
			s.modifier(style.modifier);
		}
	}

	pub fn nick(&self) -> Option<&str> {
		match self.name.part(0) {
			Some(Text::Raw(nick)) |
			Some(Text::Styled(nick, _)) => Some(nick),
			None => None
		}
	}

	pub fn group(&self) -> i8 {
		self.group
	}

	pub fn set_name<T: Into<Cow<'b, str>>>(&mut self, _: T) {
		//self.name.get_part_mut(0).replace(name);
	}
}

impl <'b> Dimensions for UserElement<'b> {
	fn width(&self) -> usize {
		self.name.width()
	}

	fn height(&self) -> usize {
		self.name.height()
	}

	fn resize(&mut self, area: Rect) {
		self.name.resize(area)
	}
}

impl <'b> Widget for UserElement<'b> {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		self.name.draw(area, buf);
	}
}

pub struct UserlistElement<'b> {
	pub users: ScrollableList<UserElement<'b>>,
	//pub session: Option<UserElement<'b>>>
	pub block: Block<'b>
}

impl <'b> UserlistElement<'b> {
	pub fn default() -> UserlistElement<'b> {
		UserlistElement {
			users: ScrollableList::new(1000, Order::Normal),
			block: Block::default().borders(Borders::ALL).title("Userlist")
		}
	}

	pub fn new() -> UserlistElement<'b> {
		UserlistElement::default()
	}

	pub fn add(&mut self, user: UserElement<'b>) {
		self.users.add(user);
	}

	pub fn extend(&mut self, elems: Vec<UserElement<'b>>) {
		for user in elems {
			self.add(user)
		}
	}

	pub fn remove_by_nick(&mut self, nick: &str) {
		self.users.items_mut().retain(|element| {
			match element.1.nick() {
				Some(n) => n != nick,
				None => false,
			}
		});

		self.users.dirty = true;
	}

	pub fn items(&self) -> &Vec<(Constraint, UserElement<'b>)> {
		self.users.items()
	}

	pub fn items_mut(&mut self) -> &mut Vec<(Constraint, UserElement<'b>)> {
		self.users.items_mut()
	}
}

impl <'b> Widget for UserlistElement<'b> {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		self.block.draw(area, buf);

		let inner = self.block.inner(area);
		self.users.draw(inner, buf);
	}
}