use tui::{
	layout::Rect,
	buffer::Buffer,
	style::{Style, Color},
	layout::{Layout, Direction, Constraint},
	widgets::{
		Widget, Block, Borders,
		Text
	},
};

use percent_encoding::percent_decode_str;
use super::{
	Paragraph
};

use crate::globals::Dimensions;
use crate::berrytube::Video;

/*
What to draw?
	- Index
	- Name
	- Duration

--> [ xxx | name 		| 01:02:03 ]
*/

#[derive(Clone)]
pub struct VideoElement {
	pub block: Block<'static>,
	pub title: Paragraph<'static>,
	pub duration: String,
	pub active: bool,
	pub volatile: bool,
	pub index: usize,
	pub area: Rect,
}

impl VideoElement {

	//TODO:
	pub fn new(title: String, length: String) -> VideoElement {

		/*
		let info = Text::from_parts(
			&[TuiText::raw(title), TuiText::raw(format!("{}", length))]
		);
		*/
		//Wrapper::truncate(true);

		VideoElement {
			block: Block::default().borders(Borders::BOTTOM),
			title: Paragraph::from_parts(&[
				Text::raw(percent_decode_str(&title).decode_utf8().unwrap().to_string())
			]),
			active: false,
			volatile: false,
			duration: length,
			index: 0,
			area: Rect::default()
		}
	}

	pub fn set_block(&mut self, block: Block<'static>) {
		self.block = block;
	}
}

impl From<&Video> for VideoElement {
	fn from(video: &Video) -> VideoElement {
		VideoElement::new(
			video.title().to_string(),
			video.duration_str()
		)
	}
}

impl Widget for VideoElement {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		self.resize(area);

		if self.active {
			self.block.border_style(Style::default().fg(Color::LightRed));
		} else {
			self.block.border_style(Style::default().fg(Color::Reset));
		}
		let index = format!("[{}]", self.index);
		let length = format!("[{}]", self.duration);

		let constraints = vec![
			Constraint::Length((index.len() + 2) as u16),
			Constraint::Length(1), //splitter
			Constraint::Max(area.width),
			Constraint::Length(1), //splitter
			Constraint::Length((length.len() + 2) as u16)
		];

		let areas = Layout::default()
			.constraints(constraints)
			.direction(Direction::Horizontal)
			.split(self.block.inner(area));

		self.block.draw(self.area, buf);
		self.title.draw(areas[2], buf);

		buf.set_string(areas[0].x + 1, areas[0].y, index, Style::default());
		buf.set_string(areas[4].x + 1, areas[4].y, length, Style::default());
	}
}

impl Dimensions for VideoElement {
	fn width(&self) -> usize {
		0
	}

	fn height(&self) -> usize {
		self.title.height() + 1
	}

	fn resize(&mut self, area: Rect) {
		if area == self.area {
			return;
		}

		self.area = area;

		let constraints = vec![
			Constraint::Length((format!("[{}]", self.index).len() + 2) as u16),
			Constraint::Length(1), //splitter
			Constraint::Max(area.width),
			Constraint::Length(1), //splitter
			Constraint::Length((format!("[{}]", self.duration).len() + 2) as u16)
		];

		let areas = Layout::default()
			.constraints(constraints)
			.direction(Direction::Horizontal)
			.split(self.block.inner(self.area));

		self.title.resize(self.block.inner(areas[2]));
	}
}
