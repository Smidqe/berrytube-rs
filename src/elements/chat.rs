use tui::{
	widgets::{Widget, Block, Borders},
	buffer::Buffer,
	layout::{Layout, Rect, Constraint}
};

use std::sync::Arc;

use parking_lot::Mutex;

use crate::globals::Dimensions;
use super::{ScrollableList, Paragraph, StatusbarElement, InputElement, Order};
use crate::Shared;

#[derive(Clone)]
pub struct MessageElement<'b> {
	pub text: Paragraph<'b>,
	pub area: Rect
}

impl <'b> Dimensions for MessageElement<'b> {
	fn width(&self) -> usize {
		self.area.width as usize
	}

	fn height(&self) -> usize {
		self.text.content.len()
	}

	fn resize(&mut self, area: Rect) {
		self.area = area;
		self.text.resize(self.area);
	}
}

impl <'b> Widget for MessageElement<'b> {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		self.area = area;
		self.text.draw(self.area, buf)
	}
}


pub struct ChatElement<'b> {
	pub area: Rect,
	pub layout: Layout,
	pub block: Block<'b>,

	//set this to InputElement
	pub input: Shared<InputElement<'b>>,
	pub status: Shared<StatusbarElement>,

	pub messages: ScrollableList<MessageElement<'b>>
}


impl <'b> ChatElement<'b> {
	pub fn append(&mut self, message: MessageElement<'b>) {
		self.messages.add(message)
	}

	pub fn new(input: Shared<InputElement<'b>>) -> ChatElement<'b> {
		ChatElement {
			layout: Layout::default().constraints(
				vec![Constraint::Max(100), Constraint::Length(2), Constraint::Length(1)]
			),
			messages: ScrollableList::new(1000, Order::Normal),
			block: Block::default().borders(Borders::ALL),
			status: Arc::new(Mutex::new(StatusbarElement::new())),
			input,
			area: Rect::default()
		}
	}
}

impl <'b> Widget for ChatElement<'b> {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {

		self.block.draw(area, buf);
		let inner = self.block.inner(area);

		let areas = self.layout.clone().split(inner);

		self.messages.draw(areas[0], buf);
		self.input.lock().draw(areas[1], buf);

		self.status.lock().draw(areas[2], buf);
	}
}

