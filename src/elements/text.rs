use tui::{
	widgets::{
		Widget, Text
	},
	layout::Rect,
	buffer::Buffer,
	style::Style
};

use crate::globals::Dimensions;

#[derive(PartialEq, Clone, Copy, Debug)]
pub enum Wrap {
	Char,
	Word,
	Truncate
}

pub struct Wrapper;

impl Wrapper {
	pub fn wrap(parts: &[Text], wrap: Wrap, max_width: usize) -> Vec<Row> {
		if max_width == 0 || parts.is_empty() {
			return Vec::default();
		}

		let mut results = Vec::new();
		let mut row = Row::default();

		parts.iter().for_each(|part| {
			let (text, style) = match part {
				Text::Styled(text, style) => (text, *style),
				Text::Raw(text) => (text, Style::default())
			};

			let part_length = text.len();
			let row_length = row.len();

			//part fits, append it and continue
			if row_length + part_length < max_width {
				row.parts_mut().push((text.to_string(), style));
				row.append(" ", style);
				return;
			}

			//create a copy
			let mut text_copy = text.clone().to_string();

			let mut wrap = if !text_copy.contains(' ') && wrap == Wrap::Word {
				Wrap::Char
			} else {
				wrap
			};

			while !text_copy.is_empty() {
				let mut push = false;
				let mut cutoff = None;

				if row.len() + text_copy.len() > max_width {
					let limit = max_width - row.len();

					cutoff = match wrap {
						Wrap::Char => text_copy.char_indices().rev().skip_while(|(index, _)| *index > limit ).next(),
						Wrap::Word | _ => text_copy.rmatch_indices(' ').skip_while(|(index, _)| *index > limit ).next().map(|x| (x.0, char::default()))
					};

					push = true;
				}

				let limit = match cutoff {
					Some(location) => location.0,
					None => text_copy.len()
				};

				if limit == 0 {
					warn!("..limit == 0..0, word: {}", text_copy);
				}

				//check if our first part of row has a space in the front
				//also skip adding it as it changes the limit if we need
				//to change the wrap method
				if row.is_empty() && text_copy.starts_with(' ') {
					text_copy = text_copy.trim_start().to_string();

					if text_copy.len() > max_width && !text_copy.contains(' ') {
						wrap = Wrap::Char
					}

					continue;
				}

				let text = text_copy.drain(..limit).collect::<String>();

				row.append(text.as_str(), style);

				if push {
					results.push(Row {
						parts: row.drain()
					});
				}
			}
		});

		if !row.is_empty() {
			results.push(row);
		}

		results
	}
}




#[derive(Debug, Default, Clone, PartialEq)]
pub struct Row {
	pub parts: Vec<(String, Style)>
}

impl Row {
	pub fn parts_mut(&mut self) -> &mut Vec<(String, Style)> {
		self.parts.as_mut()
	}

	pub fn len(&self) -> usize {
		let mut length = 0;

		self.parts.iter().for_each(|part| {
			length += part.0.len();
		});

		length
	}

	pub fn is_empty(&self) -> bool {
		self.parts.get(0).is_none()
	}

	pub fn drain(&mut self) -> Vec<(String, Style)> {
		self.parts.drain(..).collect()
	}

	pub fn part(&self, index: usize) -> Option<&(String, Style)> {
		self.parts.get(index)
	}

	pub fn part_mut(&mut self, index: usize) -> Option<&mut (String, Style)> {
		self.parts.get_mut(index)
	}

	pub fn append(&mut self, text: &str, style: Style) {
		let last = self.parts.last_mut();

		//if matches
		let (t, s) = match last {
			Some((text, style)) => (text, *style),
			_ => {
				self.parts.push((text.to_string(), style));
				return;
			}
		};

		if style == s {
			t.push_str(text);
		} else {
			self.parts.push((text.to_string(), style))
		}
	}
}


impl Widget for Row {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		let mut offset = 0;

		self.parts.iter().for_each(|(text, style)| {
			buf.set_string(area.x + offset, area.y, text.to_string(), *style);
			offset += text.len() as u16;
		});
	}
}


#[derive(Clone, Debug)]
pub struct Paragraph<'b> {
	pub raw_content: Vec<Text<'b>>,
	pub content: Vec<Row>,
	pub scroll_offset: usize, //row offset (scrollable),
	pub max_visible_height: usize,
	pub wrap_method: Wrap,
	pub dirty: bool
}

impl <'b> Paragraph<'b> {
	pub fn new() -> Paragraph<'b> {
		Paragraph {
			raw_content: Vec::new(),
			content: Vec::new(),
			scroll_offset: 0,
			max_visible_height: 1,
			wrap_method: Wrap::Word,
			dirty: false
		}
	}

	pub fn from_parts(parts: &[Text<'b>]) -> Paragraph<'b> {
		Paragraph {
			raw_content: parts.to_vec(),
			content: Vec::new(),
			scroll_offset: 0,
			max_visible_height: 1,
			wrap_method: Wrap::Word,
			dirty: true
		}
	}

	pub fn rows(&self) -> &Vec<Row> {
		self.content.as_ref()
	}

	pub fn part(&self, index: usize) -> Option<&Text> {
		self.raw_content.get(index)
	}

	pub fn part_mut(&mut self, index: usize) -> Option<&'b mut Text> {
		self.raw_content.get_mut(index)
	}

	pub fn shift(&mut self, offset: usize) {
		if self.scroll_offset == self.content.len() || self.scroll_offset + offset > self.content.len() {
			return;
		}

		self.scroll_offset += offset;
	}
	pub fn unshift(&mut self, offset: usize) {
		if self.scroll_offset == 0 || self.scroll_offset < offset {
			return
		}

		self.scroll_offset += offset;
	}

	pub fn as_string(&self) -> String {
		let mut result = String::new();

		self.raw_content.iter().for_each(|c| {
			result.push_str(match c {
				Text::Raw(s) => s,
				Text::Styled(s, _) => s
			})
		});

		result
	}
}

impl <'b> Dimensions for Paragraph<'b> {
	fn resize(&mut self, area: Rect) {
		self.content.clear();
		self.content = Wrapper::wrap(self.raw_content.as_slice(), self.wrap_method, area.width as usize);
	}

	fn height(&self) -> usize {
		self.content.len()
	}

	fn width(&self) -> usize {
		0
	}
}

impl <'b> Widget for Paragraph<'b> {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		if area.width == 0 {
			return;
		}

		if self.content.is_empty() && !self.raw_content.is_empty() || self.dirty {
			self.content = Wrapper::wrap(self.raw_content.as_slice(), self.wrap_method, area.width as usize)
		}

		self.max_visible_height = area.height as usize;

		let start = self.scroll_offset;
		let end = if self.scroll_offset > 0 {
			std::cmp::min(self.scroll_offset + self.max_visible_height, self.content.len())
		} else {
			self.content.len()
		};

		self.content[start..end].iter_mut().enumerate().for_each(|(index, row)| {
			let area = Rect {
				x: area.left(),
				y: area.top() + index as u16,
				width: area.width,
				height: 1
			};

			row.draw(area, buf);
		})
	}
}


impl <'b> From<&'b str> for Paragraph<'b> {
	fn from(s: &'b str) -> Paragraph<'b> {
		Paragraph::from_parts(&[Text::raw(s)])
	}
}

impl <'b> From<String> for Paragraph<'b> {
	fn from(s: String) -> Paragraph<'b> {
		Paragraph::from_parts(&[Text::raw(s)])
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	pub fn test_wrap_rows() {
		//text
		let text = vec![Text::raw("This string will be way too long for a single row, should be multiple")];
		let wrap = Wrapper::wrap(&text, Wrap::Word, 20);

		//assert
		assert_eq!(wrap, vec![
			Row {
				parts: vec![("This string will be".to_string(), Style::default())]
			},
			Row {
				parts: vec![("way too long for a".to_string(), Style::default())]
			},
			Row {
				parts: vec![("single row, should".to_string(), Style::default())]
			},
			Row {
				parts: vec![("be multiple".to_string(), Style::default())]
			}
		])
	}
}