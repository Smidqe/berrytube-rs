use tui::{
	widgets::{Widget, Text, Block, Borders},
	layout::Rect,
	buffer::Buffer,
	style::Style
};

use crate::globals::Dimensions;

pub struct InputElement<'b> {
	pub text: Option<Text<'b>>,
	pub block: Block<'b>,
	pub area: Rect,
}

impl <'b> InputElement<'b> {
	pub fn default() -> InputElement<'b> {
		InputElement {
			text: None,
			block: Block::default().borders(Borders::TOP),
			area: Rect::default()
		}
	}

	pub fn set_raw_text(&mut self, text: String) {
		self.text = Some(Text::raw(text));
	}

	pub fn set_styled_text(&mut self, text: &'b str, style: Style) {
		self.text = Some(Text::styled(text, style))
	}
}

impl <'b> Widget for InputElement<'b> {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {

		self.block.draw(area, buf);
		self.area = self.block.inner(area);

		if let Some(message) = &self.text {
			let (text, style) = match message {
				Text::Raw(s) => (s, Style::default()),
				Text::Styled(s, style) => (s, *style)
			};

			buf.set_string(self.area.left(), self.area.top(), text.to_string(), style)
		}
	}
}

impl <'b> Dimensions for InputElement<'b> {
	fn resize(&mut self, area: Rect) {
		self.area = area
	}

	fn width(&self) -> usize {
		self.area.width as usize
	}

	fn height(&self) -> usize {
		self.area.height as usize
	}
}