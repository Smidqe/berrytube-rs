use tui::{
	widgets::{Widget, Block, Borders},
	buffer::Buffer,
	layout::{Layout, Rect, Constraint, Direction},
	style::{Style, Color},
};

use super::{Paragraph, ScrollableList, Order};
use crate::globals::Dimensions;

use crate::berrytube::polls::{Poll, PollKind};

#[derive(Debug, Default, Clone)]
pub struct Checkbox {
	pub mark: char,
	pub checked: bool,
	pub disabled: bool,
}

impl Checkbox {
	pub fn new(mark: char) -> Checkbox {
		Checkbox {
			mark,
			checked: false,
			disabled: false
		}
	}

	pub fn check(&mut self, check: bool) {
		self.checked = check;
	}

	pub fn disable(&mut self) {
		self.disabled = true;
	}

	pub fn enable(&mut self) {
		self.disabled = false;
	}
}

impl Dimensions for Checkbox {
	fn resize(&mut self, _: Rect) {
	}

	fn width(&self) -> usize {
		5
	}

	fn height(&self) -> usize {
		3
	}
}

impl Widget for Checkbox {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		let mut style = if self.disabled {
			Style::default().fg(Color::Reset)
		} else {
			Style::default()
		};

		if self.checked {
			style = Style::default().fg(Color::Red);
		}

		//get middle point of the area
		let middle = (area.x + area.width / 2, area.y + area.height / 2);
		let area = Rect::new(middle.0 - 2, middle.1 - 1, self.width() as u16, self.height() as u16);

		let mut block = Block::default()
			.borders(Borders::ALL)
			.style(style);

		block.draw(area, buf);

		if self.checked {
			buf.set_string(middle.0, middle.1, self.mark.to_string(), style);
		}
	}
}

#[derive(Clone)]
pub struct OptionElement<'b> {
	pub block: Block<'b>,
	pub kind: PollKind,
	pub options: Vec<Checkbox>,
	pub votes: Option<usize>,
	pub text: Paragraph<'b>,
	pub area: Rect,
}

impl <'b> OptionElement<'b> {
	pub fn new(text: &str, kind: PollKind) -> OptionElement<'b> {
		let options = match kind {
			PollKind::Ranked => vec![Checkbox::new('1'), Checkbox::new('2'), Checkbox::new('3')],
			PollKind::Normal => vec![Checkbox::new('\u{2573}')]
		};

		OptionElement {
			block: Block::default().borders(Borders::RIGHT),
			kind,
			options,
			votes: None,
			text: Paragraph::from(text.to_string()),
			area: Rect::default()
		}
	}

	pub fn votes(&self) -> &Option<usize> {
		&self.votes
	}

	pub fn set_text(&mut self, text: &'b str) {
		self.text = text.into();
	}

	pub fn set_votes(&mut self, votes: usize) {
		self.votes = Some(votes);
	}

	pub fn disable(&mut self) {
		/*
		self.block.set_style();
		self.text.set_style();
		self.options.iter().for_each(|option| option.disable());
		*/
	}

	pub fn enable(&mut self) {
		/*
		self.block.set_style();
		self.text.set_style();
		self.options.iter().for_each(|option| option.disable());
		*/
	}
}

impl <'b> Dimensions for OptionElement<'b> {
	fn resize(&mut self, area: Rect) {
		if self.area == area {
			return;
		}

		self.area = area;
	}

	fn width(&self) -> usize {
		self.area.width as usize
	}

	fn height(&self) -> usize {
		std::cmp::max(self.text.height(), self.options.get(0).unwrap().height())
	}
}

impl <'b> Widget for OptionElement<'b> {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		self.resize(area);
		//calculate the needed space by the checkboxes
		//since we know the width/height of the checkboxes it's easy
		let mut constraints = vec![Constraint::Max(area.width)];

		self.options.iter().for_each(|checkbox| {
			constraints.push(Constraint::Length(checkbox.width() as u16));
		});

		let area = Rect::new(area.x, area.y, area.width, self.height() as u16);

		//split the area
		let areas = Layout::default()
			.direction(Direction::Horizontal)
			.constraints(constraints.as_slice())
			.split(area);

		//draw the elements
		self.block.draw(areas[0], buf);
		self.text.draw(self.block.inner(areas[0]), buf);

		//draw the checkboxes
		self.options.iter_mut().zip(areas[1..].iter()).for_each(|(check, area)| {
			check.draw(*area, buf);
		});
	}
}

#[derive(Clone)]
pub struct PollElement<'b> {
	//pub ranked: bool
	pub block: Block<'static>,
	pub title: String,
	pub options: ScrollableList<OptionElement<'b>>,
	pub disable: bool,
	pub area: Rect,
}

impl <'b> PollElement<'b> {
	pub fn new<T>(title: T, options: Vec<OptionElement<'b>>) -> PollElement<'b> where T: ToString {
		PollElement {
			title: title.to_string(),
			block: Block::default().borders(Borders::ALL),
			options: ScrollableList::from_items(options, 100, Order::Normal),
			disable: false,
			area: Rect::default()
		}
	}


	pub fn option(&self, index: usize) -> &OptionElement<'b> {
		&self.options.get(index).1
	}

	pub fn option_mut(&mut self, index: usize) -> &mut OptionElement<'b> {
		&mut self.options.get_mut(index).1
	}

	pub fn disable(&mut self) {
		self.options.items_mut().iter_mut().for_each(|option| {
			option.1.disable();
		})
	}
	pub fn enable(&mut self) {
		self.options.items_mut().iter_mut().for_each(|option| {
			option.1.enable();
		})
	}
}

impl <'b> Dimensions for PollElement<'b> {
	fn resize(&mut self, area: Rect) {
		if area == self.area {
			return;
		}

		self.area = area;
		self.options.resize(self.block.inner(area));
	}

	fn width(&self) -> usize {
		self.area.width as usize
	}

	fn height(&self) -> usize {
		self.options.items().iter().fold(0, |acc, elem| acc + elem.1.height()) + 2
	}
}

impl <'b> Widget for PollElement<'b> {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		self.resize(area);

		self.block.title(self.title.as_str()).draw(self.area, buf);
		self.options.draw(self.block.inner(self.area), buf);
	}
}

impl <'b> From<&Poll> for PollElement<'b> {
	fn from(poll: &Poll) -> PollElement<'b> {
		let options = match poll.kind() {
			PollKind::Normal => poll.votes.iter().zip(poll.options.iter()).map(|(_, text)| OptionElement::new(text, PollKind::Normal)).collect(),
			PollKind::Ranked => poll.extended.options.iter().map(|option| OptionElement::new(option.text.as_str(), PollKind::Ranked)).collect(),
		};

		PollElement::new(
			poll.title(),
			options,
		)
	}
}

pub struct PollsElement<'b> {
	pub block: Block<'b>,
	pub polls: ScrollableList<PollElement<'b>>
}

impl <'b> PollsElement<'b> {
	pub fn new() -> PollsElement<'b> {
		PollsElement {
			block: Block::default().borders(Borders::ALL).title("Polls"),
			polls: ScrollableList::new(10, Order::Normal)
		}
	}
}

impl <'b> Widget for PollsElement<'b> {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		self.block.draw(area, buf);
		self.polls.draw(self.block.inner(area), buf);
	}
}
