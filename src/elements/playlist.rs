use tui::widgets::{Block, Widget, Borders};
use tui::{
	buffer::Buffer,
	layout::{Rect, Constraint}
};
use super::{ScrollableList, VideoElement, Order};


pub struct PlaylistElement {
	pub block: Block<'static>,
	pub videos: ScrollableList<VideoElement>
}

impl PlaylistElement {
	pub fn new() -> PlaylistElement {
		PlaylistElement {
			block: Block::default().borders(Borders::ALL).title("Playlist"),
			videos: ScrollableList::new(2000, Order::Normal)
		}
	}

	pub fn add(&mut self, video: VideoElement) {
		self.videos.add(video);
	}

	pub fn insert(&mut self, index: usize, video: VideoElement) {
		self.videos.insert(index, video)
	}

	pub fn set_elements(&mut self, videos: Vec<VideoElement>) {
		self.videos = ScrollableList::from_items(videos, 2000, Order::Normal);
	}

	pub fn items(&self) -> &Vec<(Constraint, VideoElement)> {
		self.videos.items()
	}

	pub fn items_mut(&mut self) -> &mut Vec<(Constraint, VideoElement)> {
		self.videos.items_mut()
	}
}

impl Widget for PlaylistElement {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		self.block.draw(area, buf);
		self.videos.draw(self.block.inner(area), buf);
	}
}
