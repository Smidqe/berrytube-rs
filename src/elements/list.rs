use tui::{
	layout::{Rect, Constraint, Direction},
	buffer::Buffer,
	widgets::Widget,
	layout::Layout
};

use crate::globals::Dimensions;
use super::Scrollbar;

#[derive(Clone, PartialEq)]
pub enum Order {
	Normal,
	Reverse
}

/*
TODO: Cleanup this structure
*/
#[derive(Clone)]
pub struct ScrollableList<T> {
	pub items: Vec<(Constraint, T)>,
	pub order: Order,
	pub limit: usize,
	pub offset: usize,
	pub dirty: bool,
	pub area: Rect,
	pub used: usize,
	pub areas: Vec<Rect>,
	pub bar: Scrollbar,
}

impl <T> ScrollableList<T> where T: Widget + Dimensions + Clone {
	pub fn new(limit: usize, order: Order) -> ScrollableList<T> {
		ScrollableList {
			items: Vec::new(),
			offset: 0,
			limit,
			dirty: true,
			area: Rect::default(),
			order,
			used: 0,
			areas: Vec::new(),
			bar: Scrollbar::new()
		}
	}

	pub fn from_items(items: Vec<T>, limit: usize, order: Order) -> ScrollableList<T> {
		let mapped = items.iter().map(|item| {
			(Constraint::Length(0), item.clone())
		}).collect();

		ScrollableList {
			items: mapped,
			offset: 0,
			limit,
			dirty: true,
			area: Rect::default(),
			order,
			used: 0,
			areas: Vec::new(),
			bar: Scrollbar::new(),
		}
	}

	pub fn get(&self, index: usize) -> &(Constraint, T) {
		&self.items[index]
	}

	pub fn get_mut(&mut self, index: usize) -> &mut (Constraint, T) {
		&mut self.items[index]
	}

	pub fn is_empty(&self) -> bool {
		self.len() == 0
	}

	pub fn insert(&mut self, index: usize, mut item: T) {
		//resize the item to have a proper height for constraint
		if self.area.width != 0 {
			item.resize(self.area);
		}

		self.used += item.height() as usize;
		self.items.insert(index, (Constraint::Length(item.height() as u16), item));

		//keep the buffer at certain size
		if self.items.len() > self.limit {
			self.items.remove(0);
		}

		self.dirty = true;

	}

	pub fn add(&mut self, item: T) {
		self.insert(self.items.len(), item);
	}

	pub fn len(&self) -> usize {
		self.items.len()
	}

	pub fn shift(&mut self, amount: usize) {
		//prevent overshifting and shifting if we are not even using whole area
		if self.offset + amount > self.items.len() || self.used < self.area.height as usize {
			return;
		}

		//also we don't want to shift if we are already at max shift (aka len - shift == area.height)
		if self.items.len() - self.offset == self.area.height as usize {
			return;
		}

		self.offset += amount;
	}

	pub fn unshift(&mut self, amount: usize) {
		//prevent underflow
		if self.offset == 0 || amount > self.offset {
			return;
		}

		self.offset -= amount;
	}

	pub fn resize_item(&mut self, index: usize, area: Rect) {
		if let Some(n) = self.items.get_mut(index) {
			n.1.resize(area);
			n.0 = Constraint::Length(n.1.height() as u16);
		}
	}

	pub fn constraints(&self) -> Vec<Constraint> {
		self.items.iter().map(|i| i.0).collect()
	}

	pub fn items(&self) -> &Vec<(Constraint, T)> {
		&self.items
	}

	pub fn items_mut(&mut self) -> &mut Vec<(Constraint, T)> {
		&mut self.items
	}

	pub fn visible(&mut self) -> (usize, usize) {
		let length = self.items.len();
		let mut iterator = self.items.iter_mut().enumerate().rev().skip(self.offset);
		let mut range = (0, length);

		//handle the offset
		if range.1 > 0 && self.offset < range.1 && self.used > self.area.height as usize {
			range.1 -= self.offset;
		}

		let mut used = 0;

		while used < self.area.height {
			let (index, (_, elem)) = match iterator.next() {
				Some(e) => e,
				None => break
			};

			//need to implement a partial draw
			if used + elem.height() as u16 > self.area.height {
				break;
			}

			used += elem.height() as u16;
			range.0 = index;
		}

		range
	}
}

impl <T> Dimensions for ScrollableList<T> where T: Widget + Dimensions + Clone {
	fn width(&self) -> usize {
		self.area.width as usize
	}
	fn height(&self) -> usize {
		self.area.height as usize
	}

	fn resize(&mut self, area: Rect) {
		//no change in area, return
		if !self.areas.is_empty() && area == self.areas[0] || area.width == 0 {
			return;
		}

		self.areas.clear();
		self.areas.push(area);
		self.areas.extend(Layout::default()
			.constraints(vec![Constraint::Length(area.width - 1), Constraint::Length(1)])
			.direction(Direction::Horizontal)
			.split(area)
		);

		self.used = self.items.iter().fold(0, |acc, elem| acc + elem.1.height());
		self.area = if self.used > self.areas[0].height as usize {
			self.areas[1]
		} else {
			self.areas[0]
		};

		//need to resize every item as we had resized
		let area = self.area;

		self.items.iter_mut().for_each(|item| {
			item.1.resize(area);
			item.0 = Constraint::Length(item.1.height() as u16);
		});
	}
}

impl <T> Widget for ScrollableList<T> where T: Widget + Dimensions + Clone {
	fn draw(&mut self, area: Rect, buf: &mut Buffer) {
		if area.width == 0 {
			return;
		}

		self.resize(area);

		let range = self.visible();
		if range.0 > 0 {
			self.bar.set_pages(self.used / area.height as usize);
			self.bar.set_area(self.areas[2]);

			self.bar.draw(self.areas[2], buf);
		}

		let mut elements = self.items[range.0..range.1].to_vec();
		let mut constraints = elements.iter().map(|e| e.0).collect::<Vec<Constraint>>();

		//add one more that will not be used (whitespace)
		constraints.push(Constraint::Max(self.area.height));

		if self.order == Order::Reverse {
			constraints.reverse();
		}

		let mut areas = Layout::default()
			.constraints(constraints.as_slice())
			.direction(Direction::Vertical)
			.split(self.area);

		if self.order == Order::Reverse {
			areas.reverse();
		}

		elements.iter_mut().zip(areas).for_each(|(part, area)| {
			part.1.draw(area, buf);

			if part.1.height() > area.height as usize {
				warn!("Part height: {} > area height: {}", part.1.height(), area.height);
			}
		});
	}
}