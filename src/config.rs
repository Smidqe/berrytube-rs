pub struct Config {
	//general configs (TODO)
	//will include all color configs

	//COLORS
	//borders
	pub color_chat_border: usize,
	//background
	pub color_chat_bg: usize,
	//text color
	pub color_chat_text: usize,
	//rcv
	pub color_chat_rcv: usize,
	//drink
	pub color_chat_drink: usize,
	//squees
	pub color_chat_pm: usize,


	/*
	//general color, other more granural configs will override
	pub color_fg_master: usize
	pub color_bg_master: usize

	pub color_status_bg: usize
	pub color_status_fg: usize
	pub color_poll_bg: usize
	pub color_poll_fg: usize
	pub color_poll_active: usize
	pub color_poll_inactive: usize
	pub color_poll_option_voted: usize
	pub color_poll_option_hidden: usize
	pub color_poll_ranked_num_1: usize
	pub color_poll_ranked_num_2: usize
	pub color_poll_ranked_num_3: usize

	pub color_playlist_bg: usize
	pub color_playlist_fg: usize
	pub color_playlist_video_active: usize
	pub color_playlist_video_volatile: usize
	pub color_playlist_video_bg: usize
	pub color_playlist_video_fg: usize



	//plugins
	pub plugin_wut_colors: bool
	pub plugin_mpv: bool //eventually want to add video support

	//limits
	pub limit_max_messages: usize
	pub limit_max_polls: usize

	//mpv
	pub mpv_max_quality: usize
	pub mpv_max_fps: usize
	pub mpv_custom_command: String
	*/
}