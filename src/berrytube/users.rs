use serde::Deserialize;

use crate::elements::{UserlistElement, UserElement};
use crate::globals::Handler;
use crate::Shared;

use crate::{Error, Errors};

use super::session::Session;
use crate::parser::{Event, Events};

use std::convert::From;

#[derive(Clone, Copy, Deserialize, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum Group {
	Unknown = -3,
	Lurker = -2,
	Anon = -1,
	User = 0,
	Moderator = 1,
	Admin = 2
}

impl Default for Group {
	fn default() -> Group {
		Group::Lurker
	}
}

impl From<i8> for Group {
	fn from(n: i8) -> Self {
		match n {
			-2 => Group::Lurker,
			-1 => Group::Anon,
			0 => Group::User,
			1 => Group::Moderator,
			2 => Group::Admin,
			_ => Group::Unknown
		}
	}
}

impl Into<i8> for Group {
	fn into(self) -> i8 {
		match self {
			Group::Lurker => -2,
			Group::Anon => -1,
			Group::User => 0,
			Group::Moderator => 1,
			Group::Admin => 2,
			_ => -3
		}
	}
}

#[derive(Deserialize, Default, Debug)]
pub struct User {
	pub id: usize,
	pub nick: String,

	#[serde(alias="type")]
	pub group: i8,

	#[serde(skip)]
	pub berry: bool,

	#[serde(skip)]
	pub color: Option<(u8, u8, u8)>,
}

impl User {
	pub fn nick(&self) -> &str {
		self.nick.as_str()
	}

	pub fn group(&self) -> Group {
		self.group.into()
	}

	pub fn is_berry(&self) -> bool {
		self.berry
	}

	pub fn colour(&self) -> Option<(u8, u8, u8)> {
		self.color
	}

	pub fn set_berry(&mut self, berry: bool) {
		self.berry = berry;
	}

	pub fn set_group(&mut self, group: Group) {
		self.group = group.into();
	}

	pub fn set_nick(&mut self, nick: &str) {
		self.nick = nick.to_string();
	}

	pub fn set_colour(&mut self, color: (u8, u8, u8)) {
		self.color = Some(color);
		/*
		self.element.set_color(usize);
		*/
	}
}

pub struct Userlist {
	pub element: Shared<UserlistElement<'static>>,
	pub users: Vec<User>,

	//pub colours: HashMap<String, (u8, u8, u8)>,
	//pub element: Arc<Mutex<UserlistElement>>
}


impl Userlist {
	pub fn new(element: Shared<UserlistElement<'static>>) -> Userlist {
		Userlist {
			users: Vec::new(),
			element
		}
	}

	pub fn initialise(&mut self, users: Vec<User>) {
		self.users = users;
		self.element.lock().extend(
			self.users.iter().map(|user| {
				UserElement::new(user.nick(), user.group().into())
			}).collect()
		);
		self.sort();
	}

	pub fn sort(&mut self) {
		//order is admin -> moderator -> assistant -> leader -> user -> anon
		//also sorts by user nicks if groups are equal (a->z)

		self.element.lock().items_mut().sort_by(|a, b| {
			let nicks = (a.1.nick().unwrap().to_lowercase(), b.1.nick().unwrap().to_lowercase());
			let groups = (a.1.group(), b.1.group());

			if groups.0 == groups.1 {
				nicks.0.cmp(&nicks.1)
			} else {
				groups.1.cmp(&groups.0)
			}
		});
	}

	pub fn users(&self) -> &Vec<User> {
		&self.users
	}

	pub fn add(&mut self, user: User) {
		let nick = user.nick().to_owned();
		let group = user.group().into();

		//self.element.lock().add(UserElement::from(&user))

		self.users.push(user);
		self.element.lock().add(UserElement::new(nick.as_str(), group));

		self.sort();
	}

	pub fn remove(&mut self, nick: &str) {
		self.users.retain(|user| {
			user.nick() != nick
		});

		self.element.lock().items_mut().retain(|(_, user)| user.nick().unwrap() != nick);
	}

	pub fn search(&self, query: &str) -> Vec<&User> {
		self.users.iter().filter(|user| {
			user.nick().contains(query)
		}).collect()
	}

	pub fn get(&self, nick: &str) -> Option<&User> {
		let mut iter = self.users.iter().skip_while(|user| {
			user.nick() != nick
		});

		iter.next()
	}

	pub fn element(&self) -> Shared<UserlistElement<'static>> {
		self.element.clone()
	}
}

impl Handler for Userlist {
	fn handle_event(&mut self, _: &mut Session, event: Event) -> Result<(), Error> {
		match event.id {
			Events::Userlist => self.initialise(serde_json::from_value(event.payload)?),
			Events::Join => self.add(serde_json::from_value(event.payload)?),
			Events::Part => {
				let nick = match event.payload.get("nick") {
					Some(nick) => nick.as_str(),
					None => return Err(Error::new(Errors::JSON, "No nick field in userPart event"))
				};

				match nick {
					Some(nick) => self.remove(nick),
					None => return Err(Error::new(Errors::JSON, "Failed to convert a json to &str @ userPart"))
				}
			},
			_ => ()
		}

		Ok(())
	}

	fn handle_command(&mut self, _: &mut Session, _: &[String]) -> Result<(), Error> {
		/*
		match args {
			[_, "user", user],
			[_, "user", "search"],
			[_, "user", user, "berry"],
			[_, "user", user, "kick"],
			[_, "user", user, "ban"]
			[_, "user", user, "shadowban"]
			[_, "list"],
			[_, ""]
		}
		*/

		Ok(())
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::sync::Arc;
	use parking_lot::Mutex;

	fn init_userlist() -> Userlist {
		let mut list = Userlist::new(Arc::new(Mutex::new(
			UserlistElement::default()
		)));

		//add random users here
		list.add(
			User {
				id: 0,
				nick: "argh".to_string(),
				group: Group::Admin.into(),
				berry: false,
				color: None,
			}
		);
		list.add(
			User {
				id: 0,
				nick: "foo".to_string(),
				group: Group::Moderator.into(),
				berry: false,
				color: None,
			}
		);
		list.add(
			User {
				id: 0,
				nick: "bar".to_string(),
				group: Group::Moderator.into(),
				berry: false,
				color: None,
			}
		);
		list.add(
			User {
				id: 0,
				nick: "dolk".to_string(),
				group: Group::User.into(),
				berry: false,
				color: None,
			}
		);
		list.add(
			User {
				id: 0,
				nick: "asce".to_string(),
				group: Group::User.into(),
				berry: false,
				color: None,
			}
		);


		list
	}

	#[test]
	fn test_sort() {
		let mut list = init_userlist();

		//sort the elements in the list
		//only sorts them inside the UserList element
		//since that's the only place we care about
		list.sort();

		//get nicks from the element
		let lock = list.element.lock();
		let users: Vec<&str> = lock.items().iter().map(|(_, user)| {
			user.nick().unwrap()
		}).collect();


		assert_eq!(users[0], "argh");
		assert_eq!(users[1], "bar");
		assert_eq!(users[2], "foo");
		assert_eq!(users[3], "asce");
		assert_eq!(users[4], "dolk");
	}

	#[test]
	fn test_search() {
		let list = init_userlist();
		let results = list.search("oo");

		assert_eq!(results.len(), 1);
	}
}