use serde::Deserialize;
use serde_json::Value;

use std::convert::TryFrom;
use super::playlist::Source;

use crate::Error;

#[derive(Deserialize)]
pub struct Video {
	#[serde(rename = "videoid")]
	pub id: Value,

	#[serde(rename = "videolength")]
	pub length: usize,

	#[serde(rename = "videotitle")]
	pub title: String,

	#[serde(rename = "videotype")]
	pub source: String,

	#[serde(rename = "volat")]
	pub volatile: bool,

	#[serde(skip)]
	pub active: bool,
}

impl Video {
	pub fn id(&self) -> String {
		if self.id.is_number() {
			self.id.as_u64().unwrap().to_string()
		} else {
			self.id.as_str().unwrap().to_owned()
		}
	}

	pub fn length(&self) -> usize {
		self.length
	}

	pub fn title(&self) -> &str {
		self.title.as_str()
	}

	pub fn source(&self) -> Source {
		Source::from(self.source.as_str())
	}

	pub fn volatile(&self) -> bool {
		self.volatile
	}

	pub fn is_active(&self) -> bool {
		self.active
	}

	pub fn set_active(&mut self, active: bool) {
		self.active = active
	}

	pub fn duration_str(&self) -> String {
		let len = self.length as f64;
		let values = vec![
			(len / 3600.0).floor() as usize,
			(len / 60.0).floor() as usize,
			(len % 60.0).floor() as usize
		];

		let last = values.len() - 1;
		let parts: Vec<String> = values.iter().enumerate().map(|(index, v)| {
			//don't display hours if there's none
			if index == 0 && *v == 0 {
				return String::default();
			}

			let mut part = if (0..=9).contains(v) {
				format!("0{}", v)
			} else {
				format!("{}", v)
			};

			if index != last {
				part.push(':');
			}

			part
		}).collect();

		format!(
			"{}{}{}",
			parts[0],
			parts[1],
			parts[2]
		)
	}
}

impl TryFrom<&str> for Video {
	type Error = Error;

	fn try_from(s: &str) -> Result<Video, Error> {
		serde_json::from_str(s).map_err(|e| e.into())
	}
}

impl TryFrom<Value> for Video {
	type Error = Error;

	fn try_from(s: Value) -> Result<Video, Error> {
		serde_json::from_value(s).map_err(|e| e.into())
	}
}
