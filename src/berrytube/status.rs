use crossbeam_channel::Sender;
use parking_lot::Mutex;
use std::{
	thread::{self, JoinHandle},
	time::{Instant, Duration},
	sync::{Arc}
};

use crate::{
	Action, Shared,
	parser::{Event},
	Session,
	globals::Handler,
	error::{Error},
	elements::StatusbarElement,
	Mode
};

pub struct Statusbar {
	pub element: Shared<StatusbarElement>,
	pub drinks: Shared<(usize, f64)>,
	pub dpm: Option<JoinHandle<()>>,
	pub nick: String,
	pub video: Shared<(String, u128, u128)>,
	pub mode: Mode,
}

impl Statusbar {
	pub fn new(element: Shared<StatusbarElement>) -> Statusbar {
		Statusbar {
			element,
			drinks: Arc::new(Mutex::new((0, 0.0))),
			dpm: None,
			nick: String::default(),
			video: Arc::new(Mutex::new((String::default(), 0, 0))),
			mode: Mode::Normal
		}
	}

	pub fn set_video(&mut self, title: &str, duration: u128) {
		let mut lock = self.video.lock();

		lock.0 = title.to_string();
		lock.1 = duration;

		self.element.lock().update_video(
			title,
			duration,
			0
		);
	}

	pub fn set_drinks(&mut self, drinks: usize) {
		self.drinks.lock().0 = drinks;
		self.element.lock().update_drinks(
			drinks,
			0.0
		);
	}

	pub fn set_mode(&mut self, mode: Mode) {
		self.mode = mode.clone();
		self.element.lock().set_mode(mode)
	}

	//todo, allow starting the thread through hbvideodetail
	//todo, add io to force render when we change elements
	//
	pub fn start_video_thread(&mut self, io: Sender<Action>) {
		let drinks = self.drinks.clone();
		let video = self.video.clone();
		let element = self.element.clone();
		let duration = self.video.lock().1;

		self.dpm = Some(thread::spawn(move || {
			let mut done = false;
			let start = Instant::now();

			while !done {
				let elapsed = Instant::now().saturating_duration_since(start).as_millis() / 1000;
				let dpm = drinks.lock().0 as f64 / (duration as f64 / 60000.0);

				//only send the messages if we actually have drinks
				if dpm > 0.0 {
					element.lock().update_drinks(
						drinks.lock().0,
						dpm
					);
				}

				warn!("Elapsed: {}, Duration: {}", elapsed, duration);

				//handle video lengths here
				if elapsed < duration {
					video.lock().2 = elapsed;
					element.lock().set_video_elapsed(
						elapsed
					);

					match io.send(Action::Render) {
						_ => ()
					}
				}

				done = elapsed >= duration;

				if done {
					break;
				}

				thread::sleep(Duration::from_millis(1000));
			}
		}));
	}
}

impl Handler for Statusbar {
	fn handle_event(&mut self, _: &mut Session, event: Event) -> Result<(), Error> {
		match event.id {
			_ => (),
		}


		Ok(())
	}

	fn handle_command(&mut self, _: &mut Session, _: &[String]) -> Result<(), Error> {
		Ok(())
	}
}