use serde_json::Value;
use serde::{Deserialize, Serialize};

use std::convert::TryFrom;

use crate::Shared;
use crate::globals::Handler;
use crate::{Error, Errors};
use crate::Session;
use crate::parser::{Event, Events, Parser};

use crate::elements::{PollElement, PollsElement};


#[derive(Clone, PartialEq)]
pub enum PollKind {
	Normal,
	Ranked
}

#[derive(Serialize)]
pub struct Vote {
	op: usize,
	ballot: Vec<usize>
}

#[derive(Default, Deserialize, Clone)]
#[serde(default)]
pub struct RankedResult {
	pub ballots: Vec<usize>,
	pub index: usize, //the original index in votes (for some reason)

	#[serde(rename = "isExcluded")]
	pub excluded: bool,
	pub rank: usize,
}


#[derive(Default, Deserialize, Clone)]
#[serde(default)]
pub struct RankedOption {
	#[serde(rename = "isTwoThirds")]
	pub majority: bool,
	pub text: String
}


#[derive(Default, Deserialize, Clone)]
#[serde(default)]
pub struct ExtendedInfo {
	pub max_rank: usize,
	pub options: Vec<RankedOption>,
	pub results: Vec<RankedResult>
}


#[derive(Deserialize, Clone)]
pub struct Poll {
	#[serde(rename = "closePollInSeconds")]
	pub timeout: usize,
	pub creator: String,
	pub ghost: bool,
	pub id: usize,


	#[serde(default)]
	pub extended: ExtendedInfo,

	#[serde(rename = "isObscured")]
	pub obscured: bool,
	pub options: Vec<String>,

	#[serde(rename = "pollType")]
	pub format: String,

	#[serde(rename = "startedAt")]
	pub timestamp: u64,
	pub title: String,
	pub votes: Vec<Value>,

	#[serde(skip)]
	pub active: bool,
}

impl Poll {
	pub fn timeout(&self) -> usize {
		self.timeout
	}

	pub fn creator(&self) -> &str {
		self.creator.as_str()
	}

	pub fn ghost(&self) -> bool {
		self.ghost
	}

	pub fn obscured(&self) -> bool {
		self.obscured
	}

	pub fn id(&self) -> usize {
		self.id
	}

	pub fn title(&self) -> &str {
		self.title.as_str()
	}

	pub fn timestamp(&self) -> u64 {
		self.timestamp
	}

	pub fn kind(&self) -> PollKind {
		self.format.as_str().into()
	}

	pub fn active(&self) -> bool {
		self.active
	}
}

impl TryFrom<&'static str> for Poll {
	type Error = Error;
	fn try_from(s: &str) -> Result<Poll, Error> {

		warn!("Poll payload: {}", s);

		match serde_json::from_str(s) {
			Ok(s) => Ok(s),
			Err(e) => {
				warn!("Failed to convert from str -> poll: {}", e);
				Err(Error::from(e))
			}
		}
	}
}

impl TryFrom<Value> for Poll {
	type Error = Error;
	fn try_from(s: Value) -> Result<Poll, Error> {
		warn!("Poll payload: {}", s);

		match serde_json::from_value(s) {
			Ok(s) => Ok(s),
			Err(e) => {
				warn!("Failed to convert from value -> poll: {}", e);
				Err(Error::from(e))
			}
		}
	}
}


impl From<&str> for PollKind {
	fn from(s: &str) -> PollKind {
		match s {
			"ranked" => PollKind::Ranked,
			"normal" | _ => PollKind::Normal,
		}
	}
}


pub struct Polls {
	pub element: Shared<PollsElement<'static>>,
	pub active: Option<Shared<PollsElement<'static>>>,
	pub polls: Vec<Poll>
}

impl Polls {
	pub fn new(element: Shared<PollsElement<'static>>) -> Polls {
		Polls {
			element,
			polls: Vec::new(),
			active: None
		}
	}

	pub fn append(&mut self, poll: Poll) {
		self.element.lock().polls.insert(
			0,
			PollElement::from(&poll)
		);

		self.polls.insert(0, poll);
	}

	pub fn current(&self) -> &Poll {
		self.polls.get(0).unwrap()
	}

	pub fn current_mut(&mut self) -> &mut Poll {
		self.polls.get_mut(0).unwrap()
	}

	pub fn vote(&mut self, _: &mut Session, id: usize, rank: Option<usize>) -> Result<(), Error> {
		let current = self.current();

		//user forgot the rank
		if current.kind() == PollKind::Ranked && rank.is_none() {
			return Err(Error::new(Errors::Command, "Rank not present"));
		}

		let options_length = match current.kind() {
			PollKind::Ranked => current.extended.options.len(),
			PollKind::Normal => current.options.len()
		};

		if id > options_length {
			return Err(Error::new(Errors::Command, "Invalid option index"));
		}

		let mut ballot: Vec<usize> = (0..options_length).map(|_| 4).collect();

		if let Some(rank) = rank {
			if rank > 4 || rank == 0 {
				return Err(Error::new(Errors::Command, "Invalid poll rank"));
			}

			ballot[id] = rank;
		}

		//create the vote object
		let vote = Vote {
			op: id,
			ballot
		};

		//turn vote into json
		let _ = Parser::encode_event(
			"votePoll",
			&vote
		)?;

		//lock the element
		let mut lock = self.element.lock();

		//get the correct poll optio
		let poll = &mut lock.polls.get_mut(0).1;
		let checks = poll.option_mut(id);

		//get the correct checkbox
		let index = match current.kind() {
			PollKind::Ranked => rank.unwrap() - 1,
			PollKind::Normal => 0,
		};

		if let Some(check) = checks.options.get_mut(index) {
			check.check(true);
		}

		/*

		match session.socket_mut() {
			Some(socket) => {


				socket.send(Packet::new(Packets::Event, event.to_string().as_bytes()))?
			},
			None => ()
		};
		*/

		Ok(())
	}
}

impl Handler for Polls {
	fn handle_command(&mut self, session: &mut Session, args: &[String]) -> Result<(), Error> {
		let args: Vec<&str> = args.iter().map(|p| p.as_ref()).collect();

		match args.as_slice() {
			["vote", id] | [_, "vote", id] | [_, "v", id] => {
				self.vote(
					session,
					usize::from_str_radix(id, 10)?,
					None
				)?
			},
			["vote", id, rank] | [_, "vote", id, rank] | [_, "v", id, rank] => {
				self.vote(
					session,
					usize::from_str_radix(id, 10)?,
					usize::from_str_radix(rank, 10).ok()
				)?
			},
			[_, "test", "ranked"] => {
				//create ranked poll event from string
				let event = Parser::decode_event(
					r#"{
						"name": "newPoll",
						"args": [{
							"extended": {
								"maxRankCount": 4,
								"options": [
									{"isTwoThirds": true, "text": "Rincewind Rocks"},
									{"isTwoThirds": false, "text": "Suited for Success"},
									{"isTwoThirds": false, "text": "Trade Ya!"},
									{"isTwoThirds": false, "text": "Tanks for the Memories"},
									{"isTwoThirds": false, "text": "The Mean Six"},
									{"isTwoThirds": false, "text": "Pilots"},
									{"isTwoThirds": true, "text": "Mystery Box: EQG shorts pile"},
									{"isTwoThirds": true, "text": "Horse Movie"},
									{"isTwoThirds": false, "text": "Pinkie Apple Pie"},
									{"isTwoThirds": false, "text": "Mystery Box: Least recently played flavor"},
									{"isTwoThirds": false, "text": "Mystery Box: Mystery Box flavor"},
									{"isTwoThirds": false, "text": "Stare Master"}
								]
							},
							"closePollInSeconds": 0,
							"creator": "None",
							"ghost": false,
							"id": 1,
							"isObscured": true,
							"obscure": true,
							"options": [
								"Option 1",
								"Option 2",
								"Option 3"
							],
							"pollType": "ranked",
							"startedAt": 1579291453286,
							"title": "FAST POLL GOGOGOGO ZOOMER TIME",
							"votes": [
								"?",
								"?",
								"?"
							]
						}]
					}
					"#
				)?;

				self.handle_event(session, event)?
			},
			[_, "test"] => {
				//create a event
				let event = Parser::decode_event(
					r#"{
						"name": "newPoll",
						"args": [{
							"closePollInSeconds": 0,
							"creator": "Blueshift",
							"ghost": false,
							"id": 1,
							"isObscured": true,
							"obscure": true,
							"options": [
								"Option 1",
								"Option 2",
								"Option 3"
							],
							"pollType": "normal",
							"startedAt": 1579291453286,
							"title": "FAST POLL GOGOGOGO ZOOMER TIME",
							"votes": [
								"?",
								"?",
								"?"
							]
						}]
					}
					"#
				)?;

				self.handle_event(session, event)?
			},
			_ => ()
		};

		Ok(())
	}

	fn handle_event(&mut self, _: &mut Session, event: Event) -> Result<(), Error> {
		match event.id {
			Events::PollCreate => self.append(Poll::try_from(event.payload)?),
			Events::PollUpdate => (),
			Events::PollClose => (),
			_ => ()
		}

		Ok(())
	}
}