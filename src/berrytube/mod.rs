pub mod playlist;
pub mod video;
pub mod polls;
pub mod users;
pub mod session;
pub mod chat;
pub mod status;

pub use video::Video;
pub use chat::{Chat, Message, Metadata, Content};

use crate::Shared;
use crate::elements::{ChatElement, PollsElement, PlaylistElement, StatusbarElement, UserlistElement};

use polls::Polls;
use playlist::Playlist;
use users::Userlist;
use status::Statusbar;

pub struct Berrytube {
	pub chat: Chat,
	pub polls: Polls,
	pub playlist: Playlist,
	pub userlist: Userlist,
	pub status: Statusbar
}

impl Berrytube {
	pub fn new(
		chat: Shared<ChatElement<'static>>,
		polls: Shared<PollsElement<'static>>,
		playlist: Shared<PlaylistElement>,
		userlist: Shared<UserlistElement<'static>>,
		statusbar: Shared<StatusbarElement>
	) -> Berrytube {
		Berrytube {
			chat: Chat::new(chat),
			polls: Polls::new(polls),
			playlist: Playlist::new(playlist),
			userlist: Userlist::new(userlist),
			status: Statusbar::new(statusbar)
		}
	}
}

