use serde_json::Value;

use std::collections::HashMap;
use regex::Regex;
use std::convert::{TryFrom, From};

use super::video::Video;
use crate::Shared;

use crate::elements::{PlaylistElement, VideoElement};
use crate::Error;
use crate::parser::{Event, Events};
use crate::Handler;
use crate::Session;

/*
Block title could hold the [pos]/[amount]



functions:
	move(from, to)
	insert(to)
	add()
	get(idx)
	get_active()
	set_active(idx)
	videos()
	info()
	search()

*/

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum Source {
	Youtube,
	Vimeo,
	Dailymotion,
	Soundcloud,
	Livestream,
	Unknown
}

impl From<&str> for Source {
	fn from(s: &str) -> Source {
		match s {
			"yt" => Source::Youtube,
			"vimeo" => Source::Vimeo,
			_ => Source::Unknown
		}
	}
}

pub struct Info {
	pub videos: HashMap<Source, usize>,
	pub total_length: usize,
}

pub struct Playlist {
	pub videos: Vec<Video>,
	pub element: Shared<PlaylistElement>,
	pub active: usize,
}

impl Playlist {
	pub fn new(element: Shared<PlaylistElement>) -> Playlist {
		Playlist {
			videos: Vec::new(),
			element,
			active: 0,
		}
	}

	pub fn initialise(&mut self, videos: Value) -> Result<(), Error> {
		let videos = serde_json::from_value::<Vec<Video>>(videos);

		if let Err(e) = videos.as_ref() {
			warn!("Couldn't initialise playlist: {}", e);
		}

		self.videos = videos?;
		self.element.lock().set_elements(
			self.videos.iter().enumerate().map(|(index, video)| {
				let mut element = VideoElement::from(video);
				element.index = index;
				element
			}).collect()
		);
		Ok(())
	}

	pub fn delete(&mut self, index: usize) {
		let mut lock = self.element.lock();
		let elements = lock.items_mut();

		elements[index..].iter_mut().for_each(|element| {
			element.1.index -= 1;
		});

		elements.remove(index);
	}

	pub fn transfer(&mut self, from: usize, to: usize) {
		if from == to {
			return;
		}

		let reverse = to < from;

		//struct movement
		let video = self.videos.remove(from);
		self.videos.insert(to, video);

		//element movement
		let mut lock = self.element.lock();
		let elements = lock.items_mut();
		let mut video = elements.remove(from);

		//set the index of the moved video
		video.1.index = to;

		//insert it back to array
		elements.insert(to, video);

		//get the range between from..to
		let range = if !reverse {
			(from..=to)
		} else {
			((to + 1)..=from)
		};

		//update indexes for shifted elements
		elements[range].iter_mut().for_each(|element| {
			if !reverse {
				element.1.index -= 1;
			} else {
				element.1.index += 1;
			}
		});
	}

	pub fn insert(&mut self, index: usize, video: Video) {
		let mut lock = self.element.lock();

		lock.insert(index, VideoElement::from(&video));
		self.videos.insert(index, video);

		let elements = lock.items_mut();

		elements[index..].iter_mut().for_each(|element| {
			element.1.index += 1;
		});
	}

	pub fn get(&self, index: usize) -> Option<&Video> {
		self.videos.get(index)
	}

	pub fn active(&self) -> Option<&Video> {
		self.get(self.active)
	}

	pub fn set_active(&mut self, index: usize) {
		self.active = index
	}

	pub fn set_active_by_id(&mut self, id: &str) {
		if let Some(index) = self.videos.iter().position(|video| video.id().as_str() == id) {
			self.active = index;
		}
	}

	pub fn videos(&self) -> &Vec<Video> {
		self.videos.as_ref()
	}

	pub fn info(&self) -> Info {
		let mut videos = HashMap::<Source, usize>::new();
		let mut total_length = 0;

		self.videos.iter().for_each(|video: &Video| {
			*videos.entry(video.source()).or_insert(0) += 1;
			total_length += video.length();
		});

		Info {
			videos,
			total_length
		}
	}

	pub fn search(&self, query: &str) -> Vec<&Video> {
		let regex = Regex::new(query).unwrap();

		self.videos.iter().filter(|video| {
			regex.is_match(video.title())
		}).collect()
	}
}

impl Handler for Playlist {
	fn handle_event(&mut self, _: &mut Session, event: Event) -> Result<(), Error> {
		match event.id {
			Events::InitialisePlaylist => self.initialise(event.payload)?,
			Events::VideoRemove => {
				let index = event.payload.get("position").unwrap().as_u64().unwrap();
				self.delete(index as usize);
			},
			Events::VideoAdd => {
				self.insert(self.active + 1, Video::try_from(event.payload.get("video").unwrap().to_owned())?)
			},
			Events::VideoMove => {
				let from = event.payload.get("from").unwrap().as_u64().unwrap() as usize;
				let to = event.payload.get("to").unwrap().as_u64().unwrap() as usize;

				self.transfer(from, to);
			},
			Events::HBVideoDetail => {
				//no idea
			},
			_ => ()
		};

		Ok(())
	}
	fn handle_command(&mut self, _: &mut Session, args: &[String]) -> Result<(), Error> {
		let args: Vec<&str> = args.iter().map(|p| p.as_ref()).collect();

		match args.as_slice() {
			[_, "test", "swap", from, to] => {
				let from = usize::from_str_radix(from, 10)?;
				let to = usize::from_str_radix(to, 10)?;

				self.transfer(from, to);
			}
			_ => ()
		}

		Ok(())
	}
}