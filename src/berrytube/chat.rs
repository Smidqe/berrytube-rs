use tui::widgets::Text;
use tui::style::Style;

use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::globals::Handler;
use crate::parser::{Event, Events};


use crate::{
	Shared,
	elements::{ChatElement, MessageElement, Paragraph},
	Error,
	Errors,
	Session,
	Parser,
	Packets,
	socket::packet::Packet
};

use tui::layout::Rect;

#[derive(PartialEq)]
pub enum Emote {
	Normal,
	Drink,
	RCV,
	Request
}

impl From<&str> for Emote {
	fn from(v: &str) -> Emote {
		match v {
			"request" => Emote::Request,
			"drink" => Emote::Drink,
			"rcv" => Emote::RCV,
			_ => Emote::Normal
		}
	}
}

#[derive(PartialEq, Eq, Hash)]
pub enum Buffer {
	Normal,
	Moderator,
	Log
}

#[derive(Serialize, Deserialize)]
pub struct OutboundMeta {
	pub flair: String,
	pub channel: String,
}

#[derive(Serialize, Deserialize)]
pub struct Outbound {
	pub msg: String,
	pub metadata: OutboundMeta
}

#[derive(Deserialize, Default)]
pub struct Metadata {
	#[serde(default)]
	pub channel: String,
	pub flair: Value, //TODO: Fix this when possible
	#[serde(rename = "nameflaunt")]
	pub flaunt: bool
}

impl Metadata {
	pub fn channel(&self) -> &str {
		self.channel.as_str()
	}

	pub fn flair(&self) -> u64 {
		self.flair.as_u64().unwrap()
	}

	pub fn flaunt(&self) -> bool {
		self.flaunt
	}
}

#[derive(Deserialize, Default)]
pub struct Content {
	//can be a false or String (pls)
	//#[serde(default = "Emote::default")]
	pub emote: Value,
	#[serde(rename = "metadata")]
	pub meta: Metadata,
	#[serde(rename = "msg")]
	pub message: String,
	pub multi: usize,
	#[serde(rename = "nick")]
	pub sender: String,
	pub timestamp: String,

	#[serde(rename = "type")]
	pub format: i8
}

impl Content {
	pub fn emote(&self) -> Value {
		self.emote.clone()
	}

	pub fn meta(&self) -> &Metadata {
		&self.meta
	}

	pub fn message(&self) -> &str {
		self.message.as_str()
	}

	pub fn multi(&self) -> usize {
		self.multi
	}

	pub fn sender(&self) -> &str {
		self.sender.as_str()
	}

	pub fn timestamp(&self) -> &str {
		self.timestamp.as_str()
	}

	pub fn format(&self) -> i8 {
		self.format
	}
}

#[derive(Deserialize, Default)]
pub struct Message {
	pub ghost: bool,

	#[serde(rename = "msg")]
	pub content: Content,
}

impl Message {
	pub fn ghost(&self) -> bool {
		self.ghost
	}

	pub fn content(&self) -> &Content {
		&self.content
	}
}

pub struct Chat {
	pub messages: Vec<Message>,
	pub element: Shared<ChatElement<'static>>
}

impl Chat {
	pub fn new(element: Shared<ChatElement<'static>>) -> Chat {
		Chat {
			messages: Vec::new(),
			element
		}
	}

	pub fn append(&mut self, message: Value) -> Result<(), Error> {
		let message: Message = serde_json::from_value(message)?;

		//TODO: Make limit configurable
		if self.messages.len() > 100 {
			self.messages.remove(0);
		}

		let mut parts = Vec::new();

		//get the type of the message
		let format = match message.content.emote.as_str() {
			Some(s) => Emote::from(s),
			None => Emote::Normal
		};

		//add nick
		parts.push(Text::styled(format!("{}:", message.content.sender()), Style::default()));

		//if request add "requests"
		if format == Emote::Request {
			parts.push(Text::styled("requests", Style::default()));
		}

		//add main message
		parts.push(Text::raw(message.content.message().to_owned()));

		//add drink call after, if you wanted a drink
		if format == Emote::Drink {
			parts.push(Text::styled("drink", Style::default()))
		}

		//create the element itself for the chat element
		self.element.lock().append(MessageElement {
			text: Paragraph::from_parts(parts.as_slice()),
			area: Rect::default() //remove this eventually by creating MessageElement::from_paragraph()
		});

		//save it to our buffer
		self.messages.push(message);

		//TODO: As with element, keep a limited amount of messages

		Ok(())
	}

	pub fn send(&self, session: &mut Session, msg: &str) -> Result<(), Error> {
		//create the outbound message
		let outbound = Outbound {
			msg: msg.to_owned(),
			metadata: OutboundMeta {
				channel: session.channel().to_owned(),
				flair: session.flair().to_owned()
			}
		};

		//serialize it to socket.io format
		let event = Parser::encode_event(
			"chat",
			&outbound
		)?;

		//send the encoded message
		session.socket_mut().unwrap().send(
			Packet::new(Packets::Event, event.to_string().as_bytes())
		)?;

		Ok(())
	}

}

impl Handler for Chat {
	fn handle_event(&mut self, _: &mut Session, event: Event) -> Result<(), Error> {
		match event.id {
			Events::Message => self.append(event.payload),
			_ => Err(Error::new(Errors::Custom, format!("No even handler for event: {:?}", event).as_str()))
		}
	}

	fn handle_command(&mut self, _: &mut Session, _: &[String]) -> Result<(), Error> {
		/*
		match args {
			[_, "search", "nick", nick]
			[_, "users"]
			[_, ]
		}
		*/

		Ok(())
	}
}