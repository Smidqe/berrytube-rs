use serde::Serialize;
use crate::SocketIO;

use super::users::User;
use crate::socket::{
	Packets,
	packet::Packet
};

use crate::Mode;

use crate::{
	error::{Errors, Error},
	parser::Parser
};

#[derive(PartialEq)]
pub enum State {
	Unconnected,
	Connected,
	Logged,
	Kicked,
	Banned,
}

impl Default for State {
	fn default() -> State {
		State::Unconnected
	}
}

#[derive(Serialize)]
struct Login {
	nick: String,
	#[serde(default = "bool::default")]
	pass: Option<String>
}

#[derive(Default)]
pub struct Session {
	pub user: Option<User>,
	pub state: State,
	pub socket: Option<SocketIO>,
	pub mode: Mode
}


impl Session {
	pub fn new() -> Session {
		Session {
			user: None,
			state: State::Unconnected,
			socket: None,
			mode: Mode::Normal
		}
	}

	pub fn with_socket(socket: SocketIO) -> Session {
		Session {
			user: None,
			state: State::Unconnected,
			socket: Some(socket),
			mode: Mode::Normal
		}
	}

	pub fn connect(&mut self) {
		unimplemented!()
	}

	pub fn disconnect(&mut self) -> Result<(), Error> {
		match self.socket.take() {
			Some(mut socket) => socket.stop(),
			None => Err(Error::new(Errors::Io, "Failed to disconnect"))
		}
	}

	pub fn mode(&self) -> &Mode {
		&self.mode
	}

	pub fn set_mode(&mut self, mode: Mode) {
		self.mode = mode
	}

	pub fn login(&mut self, args: &[String]) -> Result<(), Error> {
		if self.socket.is_none() {
			return Err(Error::new(Errors::Login, "We are not connected, can't login"))
		}

		let password = match args {
			[_, _, pass] => Some(pass.to_string()),
			[_, _] => None,
			_ => return Err(Error::new(Errors::Login, "Invalid args"))
		};

		let login = Login {
			nick: args[1].to_string(),
			pass: password
		};

		if let Some(socket) = &mut self.socket {
			let event = Parser::encode_event(
				"setNick",
				&login
			)?;

			socket.send(
				Packet::new(Packets::Event, event.to_string().as_bytes())
			)?;
		}

		Ok(())
	}

	pub fn flair(&self) -> &str {
		"0"
	}

	pub fn channel(&self) -> &str {
		"main"
	}

	pub fn state(&self) -> &State {
		&self.state
	}

	pub fn user(&self) -> Option<&User> {
		self.user.as_ref()
	}

	pub fn user_mut(&mut self) -> Option<&mut User> {
		self.user.as_mut()
	}

	pub fn socket(&self) -> Option<&SocketIO> {
		self.socket.as_ref()
	}

	pub fn socket_mut(&mut self) -> Option<&mut SocketIO> {
		self.socket.as_mut()
	}
}