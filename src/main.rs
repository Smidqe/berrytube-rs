extern crate crossbeam_channel;

extern crate tui;
extern crate tungstenite;
extern crate openssl;
extern crate parking_lot;
extern crate serde_json;
extern crate log4rs;
extern crate regex;

#[macro_use]
extern crate log;
extern crate crossterm;
extern crate serde;

use crossbeam_channel::{unbounded};
use std::sync::Arc;
use parking_lot::Mutex;

use berrytube::Berrytube;

pub type Shared<T> = Arc<Mutex<T>>;

pub mod socket;
pub mod berrytube;
pub mod components;
pub mod error;
pub mod ui;
pub mod globals;
pub mod commands;
pub mod elements;
pub mod parser;
pub mod util;

use berrytube::session::{Session, State};

use socket::adapter::SocketIO;
use error::{Error, Errors};
use ui::{UI};
use socket::{
	Packets
};


use parser::{Parser, Events, Commands};

use globals::{Action, Mode, Handler};
use components::input::Input;

fn main() -> Result<(), Error> {
	//setup necessary variables
	let io = unbounded::<Action>();

	//create the session with the socket
	//TODO: Make socket connectable later instead of right away
	let mut session = Session::with_socket(
		SocketIO::new("socket.berrytube.tv", "socket.io", io.0.clone())?
	);

	let mut ui = UI::new()?;
	let mut bt = Berrytube::new(
		ui.chat.clone(),
		ui.polls.clone(),
		ui.playlist.clone(),
		ui.userlist.clone(),
		ui.statusbar.clone()
	);

	//will be part of Berrytube struct, probably
	let mut input = Input::new(io.0.clone(), ui.input.clone());

	//run the input thread
	input.run();

	//setup logging
	log4rs::init_file("log.yaml", Default::default()).unwrap();

	//do the initial render
	match ui.render() {
		_ => ()
	};

	let mut cursor = (0, 0);
	loop {
		let mut render = false;

		match io.1.recv() {
			//TODO: Handle restarts
			Ok(Action::Shutdown) | Err(crossbeam_channel::RecvError) => {
				break;
			},
			//Will handle majority of the key related events
			Ok(Action::Key(key)) => {
				warn!("Pressed key: {:?}", key);
				/*
				match key.code {
					KeyCode::Tab => ui.focus_next(),
					KeyCode::BackTab => ui.focus_prev(),
					KeyCode::PageUp => ui.scroll(Scroll::Up, -1),
				}
				*/
			},
			Ok(Action::Mode(mode)) => {
				bt.status.set_mode(mode.clone());
				session.set_mode(mode);
			},
			Ok(Action::InputBuffer(buf)) => {
				input.set_visible_str(buf.as_str())
			},
			Ok(Action::Message(msg)) => {
				match session.mode() {
					Mode::Chat =>  {
						if *session.state() == State::Logged {
							if let Err(e) = bt.chat.send(&mut session, msg.as_str()) {
								warn!("Failed to send message: {}", e);
							}
						}
					},
					Mode::Command => {
						if let Ok(command) = Parser::parse_command(&msg) {
							let oper = match command.command {
								Commands::Login => session.login(command.args.as_slice()),
								Commands::UI => ui.handle_command(&mut session, command.args),
								Commands::Polls => bt.polls.handle_command(&mut session, command.args.as_slice()),
								Commands::Playlist => bt.playlist.handle_command(&mut session, command.args.as_slice()),
								Commands::Quit => Ok(()),


								Commands::Unknown => Err(Error::new(Errors::Command, "Unknown command")),
								_ => Err(Error::new(Errors::Command, &format!("Command: {} doesn't exist", command.args[0])))
							};

							if let Err(e) = oper {
								warn!("Command failed: {}", e);
							}
						}
					}
					_ => ()
				}

				render = true;
			},
			Ok(Action::Event(packet)) => {
				//we don't care about the other events
				if packet.id != Packets::Event {
					continue
				}

				if let Ok(event) = Parser::decode_event(&String::from_utf8(packet.data).unwrap()) {
					let id = event.id.clone();

					if event.name != "hbVideoDetail" {
						render = true;
					}

					let result = match event.id {
						//chat related events
						Events::Message => bt.chat.append(event.payload), //bt.chat.handle_event(event.payload)

						//playlist related events
						Events::InitialisePlaylist | Events::VideoAdd | Events::VideoMove | Events::VideoRemove |
						Events::Volatility | Events::HBVideoDetail => bt.playlist.handle_event(&mut session, event),

						//userlist related events
						Events::Join | Events::Part | Events::Usercount | Events::Berry |
						Events::Userlist | Events::Type | Events::Nick => bt.userlist.handle_event(&mut session, event),

						//poll related events
						Events::PollCreate | Events::PollUpdate | Events::PollClose => bt.polls.handle_event(&mut session, event),

						//Special case for video change
						Events::VideoChange => {
							let title = event.payload["video"]["videotitle"].as_str().unwrap();
							let length = event.payload["video"]["videolength"].as_u64().unwrap();

							//handle things for statusbar
							bt.status.set_video(title, length as u128);
							bt.status.start_video_thread(io.0.clone());

							bt.playlist.handle_event(&mut session, event)
						}

						//drink related events (only statusbar event)
						Events::Drinks => Ok(()),

						//Ignored events
						Events::Areas | Events::Toggleables => Ok(()),

						_ => {
							Err(Error::new(Errors::JSON, format!("Event handling for: {} not implemented", event.name).as_str()))
						}
					};

					if let Err(e) = result {
						warn!("Handling event: {:?} failed: {}", id, e);
					}
				}
			}
			//we got cursor event, only render the cursor, not the whole thing
			Ok(Action::Cursor(c)) => {
				cursor = c;
			},
			Ok(Action::Render) => render = true,
			_ => ()
		};

		if render {
			ui.render()?
		}

		ui.set_cursor(cursor)?;
	}

	session.disconnect()?;
	ui.stop()?;

	Ok(())
}
