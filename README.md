# WORK IN PROGRESS

# What?
A TUI based client for berrytube.tv made in Rust.

# Why?
This project started after a failed attempt on converting Socket.IO library to Rust.
As there already existed an IRC bridge for this website and an very old Java client
that probably doesn't don't work anymore. Because of those circumstances and also wanting to acquire a deeper understanding of Rust led me to create this chat client.

# Implemented features
- Vim-like command structure


# Commands
To utilise these commands you must be in Command mode, which will be indicated on the statusbar on the bottom of the program. Command mode is accessed from Normal mode
and is automatically switched when `:` is written to input.

- `:q - Quit the program`
- `:login [nick] [pass] - Login to berrytube.tv using your credentials (currently not operational until command handling is implemented for it)`


# To be implemented
- Rest of the elements
  - Playlist
	- Video
  - Polls
    - Poll option
      - Checkbox
    - Ranked option
    -
- Playlist integration
- Poll integration
  - Normal single vote polls
  - Ranked polls
- Userlist integration
- Wutcolors integration
- Tests

# Planned features
- Customisable styling for entire program
- Moderator operations integration
- MPV integration for video support

